#!/usr/bin/env python

import commands
import os
import shutil
import sys

def toJPG(source):
    target = source[:-3]+'jpg'
    execute('convert -density 300 '+source+' '+target)
    return target

def execute(command):
    status,output = commands.getstatusoutput(command)
    if status != 0:
        print command,output

def header():
    return [
        '\\documentclass{article}',
        '\\usepackage{pstricks}',
        '\\usepackage{pst-3dplot}',
        '\\pagestyle{empty}',
        '\\begin{document}' ]

def footer():
    return [ '\\end{document}' ]

def pspicture(contents,width=10,height=10,border=True,borderColor='white'):
    retval = [ ]
    retval.append('\\begin{pspicture}('+str(-.5*width)+','+str(-.5*height)
                +')('+str(0.5*width)+','+str(0.5*height)+')')
    if border:
        retval.append('\\psframe[linecolor='+borderColor+',fillcolor='
            +borderColor+']('+str(-.5*width)+','+str(-.5*height)
                     +')('+str(0.5*width)+','+str(0.5*height)+')')
    retval += contents
    retval.append('\\end{pspicture}')
    return retval

def run(name,config):
    image = getattr(__import__(name,fromlist=['image']),'image')
    opts  = getattr(__import__(name,fromlist=['opts']),'opts')
    files = [ ]
    jpgs  = [ ]
    for i in range(opts['steps']):
        file = makeImage(image,opts,i)
        files.append(file)
        jpgs.append(toJPG(file))
    gifFile = name+'.gif'
    print gifFile
    execute(' '.join(['convert']+jpgs+[gifFile]))
#for entry in os.listdir(config['tmpDir']):
#        if os.path.basename(entry).startswith('tmp'):
#            os.remove(os.path.join(config['tmpDir'],entry))

def makeImage(image,opts,N):
    contents = image(float(N))
    if not type(contents) == list:
        raise Exception('invalid return value')
    lines = header() + pspicture(contents,
        opts['width'],opts['height'],
        opts['border'],opts['borderColor']) + footer()
    texFile = os.path.join(config['tmpDir'],config['tmpName']+'.tex')
    pdfFile = os.path.join(config['tmpDir'],config['tmpName']+'.pdf')
    with open(texFile,'w') as file:
         for line in lines:
            file.write(line+'\n')
    execute('make -C '+config['tmpDir']+'/')
    targetFile = os.path.join(config['tmpDir'],config['tmpName']+str(N)+'.pdf')
    shutil.move(pdfFile,targetFile)
    return targetFile

config = {
    'tmpDir' : './tmp',
    'tmpName' : 'tmp'
}

for arg in sys.argv[1:]:
    run(arg,config)
