import math

opts = { 
    'width'  : 10,
    'height' : 10,
    'border' : True,
    'borderColor' : 'red',
    'steps'  : 60,
}

def image(N):
    retval = [ ]
    alpha = 6*N
    beta = 30
    retval.append('\\psset{Alpha='+str(alpha)+',Beta='+str(beta)+'}')
    retval += [
        '\pstThreeDLine[linecolor=blue]{->}(0,0,0)(0,0,2)',
        '\pstThreeDLine[linecolor=black]{->}(0,0,0)(0,2,0)',
        '\pstThreeDLine[linecolor=black]{->}(0,0,0)(2,0,0)',
    ]
    l = math.sin(2*3.141*1./60*N)
    retval.append('\pstThreeDLine[linecolor=red]{->}(0,0,0)('+str(l)+',0,0)')
    return retval
