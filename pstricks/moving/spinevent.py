import math
from helper import *

opts = {
    'width'  : 10,
    'height' : 10,
    'border' : True,
    'borderColor' : 'red',
    'steps'  : (7+8)*20,
}

def image(N):
    retval = [ ]
    
    spd = 20

    spd0 = spd
    spd1 = spd + spd0
    spd2 = spd + spd1
    spd3 = spd + spd2
    spd4 = 2*spd + spd3
    spd5 = spd + spd4
    spd6 = 8*spd + spd5

    if N in range(spd5,spd6):
        x = (2./(spd6-spd5)) * (N-spd5)
        alpha = 60 + x*360
        retval.append(setCoor(alpha,30))
    else:
        retval.append(setCoor(60,30))
    
    origin = vector(0,0,0,0)


# reference frame
    rU = vector(0,0,4,0)
    rB = rU.scale(-1)
    rT = vector(0,0,1.8,1)
    rS = rT.scale(-4/rT.abs())
    rT.setM(172)

# top-frame
    tB = vector(0,0,0,-4)
    tW = tB.scale(-0.45)
    tW.setM(80)
    tB.setM(4)

# W-frame
    wL = vector(0,0.6,-0.2,-1.5)
    wL.setM(0.1)
    wN = wL.scale(-1)

# Axes
    aX = vector(0,4,0,0)
    aY = vector(0,0,4,0)
    aZ = vector(0,0,0,4)

    if N in range(0,spd0):
        vScale = (1./spd0)*(spd0-N)
        retval += [
            line(rU.scale(vScale),arrow=True,color='black',start=rU),
            line(rB.scale(vScale),arrow=True,color='black',start=rB),
        ]
        if vScale < 0.95:
            retval += [
                put(rU.scale(vScale).offset(0,0,-.25),'\\small $u$'),
                put(rB.scale(vScale).offset(0,0,-.25),'\\small $b$'),
            ]
    if N in range(spd0,spd1):
        vScale = (1./(spd1-spd0)) * (N-spd0)
        retval += [
            line(rU,color='gray'),
            line(rB,color='gray'),
            line(rT.scale(vScale),arrow=True,color='green'),
            line(rS.scale(vScale),arrow=True,color='black'),
            dot(origin),
        ]
    if N in range(spd1,spd2):
        vScale = (1./(spd2-spd1)) * (N-spd1)
        bW = tW.boost(rT.scale(-1))
        bB = tB.boost(rT.scale(-1))
        retval += [
            line(rU,color='lightgray'),
            line(rB,color='lightgray'),
            line(rT,arrow=True,color='green'),
            line(rS,arrow=True,color='black'),
            line(bW.scale(vScale).add(rT),arrow=True,color='blue',start=rT),
            line(bB.scale(vScale).add(rT),arrow=True,color='black',start=rT),
            dot(origin),
        ]
    if N in range(spd2,spd3):
        bScale = (1./(spd3-spd2)) * (N-spd2)
        rBoost = rT.scale(bScale)
        tBoost = rT.scale(-1+bScale)
        bT = rT.boost(rBoost)
        bS = rS.boost(rBoost)
        bW = tW.boost(tBoost)
        bB = tB.boost(tBoost)
        retval += [
            line(rT,arrow=True,color='red'),
            line(bT,arrow=True,color='green'),
            line(bS,arrow=True,color='black'),
            line(bW.add(bT),arrow=True,color='blue',start=bT),
            line(bB.add(bT),arrow=True,color='gray',start=bT),
        ]
    if N in range(spd3,spd4):
        bS = rS.boost(rT)
        bL = wL.boost(tW)
        bN = wN.boost(tW)
        x = (1./(spd4-spd3)) * (N-spd3)
        if x > 0.5:
            retval += [
                line(rT,start=rT.projY(),color='gray',dashed=True),
                line(rT,start=rT.projZ(),color='gray',dashed=True),
            ]
        if x > 0.15:
            retval += [
                line(aX,arrow=True),
            ]
        if x > 0.30:
            retval += [
                line(aY,arrow=True),
            ]
        if x > 0.75:
            retval += [
                line(bL.add(tW),color='blue',arrow=True,start=tW),
                line(bN.add(tW),color='gray',arrow=True,start=tW),
            ]
        retval += [
            line(rT,arrow=True,color='red'),
            line(bS,arrow=True,color='gray'),
            line(tW,arrow=True,color='blue'),
            line(tB,arrow=True,color='lightgray'),
        ]
        retval.append(dot(origin,color='green'))
    if N in range(spd4,spd5):
        bScale = (1./(spd5-spd4)) * (N-spd4)
        tBoost = tW.scale(bScale)
        wBoost = tW.scale(-1+bScale)
        bT = rT.boost(rT).boost(tBoost)
        bS = rS.boost(rT).boost(tBoost)
        bW = tW.boost(tBoost)
        bB = tB.boost(tBoost)
        bL = wL.boost(wBoost)
        bN = wN.boost(wBoost)
        retval += [
            line(rT,start=rT.projY(),color='gray',dashed=True),
            line(rT,start=rT.projZ(),color='gray',dashed=True),
            line(aX,arrow=True),
            line(aY,arrow=True),
            line(aZ,arrow=True),
            line(rT,arrow=True,color='red'),
            line(bS.add(bT),arrow=True,color='gray',start=bT),
            line(bB,arrow=True,color='lightgray'),
            line(bW,arrow=True,color='blue'),
            line(bT,arrow=True,color='green'),
            line(bL.add(bW),arrow=True,color='blue',start=bW),
            line(bN.add(bW),arrow=True,color='gray',start=bW),
            line(wL.boost(tW).add(bW),arrow=True,color='gray',start=bW,dashed=True)
        ]
    if N in range(spd5,spd6):
        bT = rT.boost(rT).boost(tW)
        bS = rS.boost(rT).boost(tW)
        bW = tW.boost(tW)
        bB = tB.boost(tW)
        retval += [
            line(rT,start=rT.projY(),color='gray',dashed=True),
            line(rT,start=rT.projZ(),color='gray',dashed=True),
            line(rT,arrow=True,color='red'),
            line(aX,arrow=True),
            line(aY,arrow=True),
            line(aZ,arrow=True),
            line(bS.add(bT),arrow=True,color='gray',start=bT),
            line(bB,arrow=True,color='lightgray'),
#        line(bW,arrow=True,color='blue'),
            line(bT,arrow=True,color='green'),
            line(wL,arrow=True,color='blue'),
            line(wN,arrow=True,color='gray'),
            line(wL.boost(tW),arrow=True,color='gray',dashed=True),
            dot(origin,color='blue')
        ]

    return retval
