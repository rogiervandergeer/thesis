import math

opts = { 
    'width'  : 10,
    'height' : 10,
    'border' : True,
    'borderColor' : 'red',
    'steps'  : 60,
}

def image(N):
    retval = [ ]
    alpha = 6*N
    beta = 30
    retval.append('\\psset{Alpha='+str(alpha)+',Beta='+str(beta)+'}')
    retval += [
        '\pstThreeDLine[linecolor=gray,linestyle=dashed](0,0.7,0)(0,0.7,1.4)',
        '\pstThreeDLine[linecolor=gray,linestyle=dashed](0,0,1.4)(0,0.7,1.4)',
        '\pstThreeDLine[linecolor=blue]{->}(0,0,0)(0,0,2)',
        '\pstThreeDLine[linecolor=black]{->}(0,0,0)(0,2,0)',
        '\pstThreeDLine[linecolor=black]{->}(0,0,0)(2,0,0)',
        '\pstThreeDLine[linecolor=red]{->}(0,0,0)(0,0.7,1.4)',
        '\pstThreeDDot(0,0,0)',

        '\pstThreeDPut(0,0,2.25){\scriptsize $\\vec{p}_{\\rm w}$}',
        '\pstThreeDPut(0,2.25,0){\scriptsize $\\vec{T}$}',
        '\pstThreeDPut(2.25,0,0){\scriptsize $\\vec{N}$}',
        '\pstThreeDPut(0,0.86,1.56){\scriptsize $\\vec{s}_{\\rm t}$}',

    ]
#    l = math.sin(2*3.141*1./60*N)
#    retval.append('\pstThreeDLine[linecolor=red]{->}(0,0,0)('+str(l)+',0,0)')
    return retval
