import math
from helper import *

opts = { 
    'width'  : 10,
    'height' : 10,
    'border' : True,
    'borderColor' : 'red',
    'steps'  : 60,
}

def image(N):
    N = float(N)
    retval = [ ]
    alpha = 60
    beta = 30
    retval.append('\\psset{Alpha='+str(alpha)+',Beta='+str(beta)+'}')
    vW = vector(0,0,0,2)
    vW.setM(80)
    vt = vector(0,0,0,0)
    vt.setM(172)
    vN = vector(0,1,0,0)
    vT = vector(0,0,1,0)
    vO = vector(0,0,0,0)
    vS = vector(0,0,0.7,1.4)
    bV = vW.scale(N/60)
    retval += [
        line(vS,color='gray',dashed=True,start=vS.projY()),
        line(vS,color='gray',dashed=True,start=vS.projZ()),
        line(vW.boost(bV),color='blue',arrow=True),
        line(vN,arrow=True),
        line(vT,arrow=True),
        line(vS,arrow=True,color='red'),
        dot(vO),
        put(vW.scale(1.12),'\\scriptsize $\\vec{p}_{\\rm w}$'),
        put(vT.scale(1.12),'\\scriptsize $\\vec{N}$'),
        put(vN.scale(1.12),'\\scriptsize $\\vec{T}$'),
        put(vS.scale(1.12),'\\scriptsize $\\vec{s}_{\\rm t}$'),
    ]
#    l = math.sin(2*3.141*1./60*N)
#    retval.append('\pstThreeDLine[linecolor=red]{->}(0,0,0)('+str(l)+',0,0)')
    return retval
