from math import sqrt

class vector(object):
    def __init__(self,E,px,py,pz):
        self.px = px
        self.py = py
        self.pz = pz
        self.E  = E
        if E < self.abs():
            self.setM(0)
    def setM(self,M):
        self.E = sqrt(self.abs()*self.abs()+M*M)
    def M(self):
        return sqrt(self.E*self.E - self.abs()*self.abs())
    def p(self):
        print self.E,self.px,self.py,self.pz
    def abs(self):
        return sqrt(self.px*self.px + self.py*self.py + self.pz*self.pz)
    def str(self):
        return '('+str(self.px)+','+str(self.py)+','+str(self.pz)+')'
    def projX(self):
        return vector(self.E,self.px,0,0)
    def projY(self):
        return vector(self.E,0,self.py,0)
    def projZ(self):
        return vector(self.E,0,0,self.pz)
    def scale(self,factor):
        V = vector(self.E,self.px*factor,
                self.py*factor,self.pz*factor)
        V.setM(self.M())
        return V
    def boost(self,boostVector):
        if boostVector.E == 0:
            return self.scale(1)
        if boostVector.M() < 1E-10:
            bX = boostVector.px
            bY = boostVector.py
            bZ = boostVector.pz
        else:
            bX = boostVector.px/boostVector.E
            bY = boostVector.py/boostVector.E
            bZ = boostVector.pz/boostVector.E
        b = sqrt(bX*bX + bY*bY + bZ*bZ)
        if b == 0:
            return self.scale(1)
        if b >= 1:
            raise Exception('boost > 1!')
        g = 1./sqrt(1-b*b)
        nE = g*(self.E - bX*self.px - bY*self.py - bZ*self.pz)
        nX = -g*bX*self.E + (1+(g-1)*bX*bX/(b*b))*self.px +\
             (g-1)*bX*bY/(b*b)*self.py + (g-1)*bX*bZ/(b*b)*self.pz
        nY = -g*bY*self.E + (1+(g-1)*bY*bY/(b*b))*self.py +\
             (g-1)*bX*bY/(b*b)*self.px + (g-1)*bZ*bY/(b*b)*self.pz
        nZ = -g*bZ*self.E + (1+(g-1)*bZ*bZ/(b*b))*self.pz +\
             (g-1)*bX*bZ/(b*b)*self.px + (g-1)*bZ*bY/(b*b)*self.py
        return vector(nE,nX,nY,nZ)
    def offset(self,ox,oy,oz):
        V = vector(0,ox+self.px,oy+self.py,oz+self.pz)
        V.setM(self.M())
        return V
    def add(self,v):
        V = vector(0,v.px+self.px,v.py+self.py,v.pz+self.pz)
        V.setM(self.M())
        return V


def line(vect,color='black',dashed=False,start=None,arrow=False):
    retval = '\\pstThreeDLine'
    options = [ ]
    if color:
        options += [ 'linecolor='+color ]
    if dashed:
        options += [ 'linestyle=dashed' ]
    retval += '[' + ','.join(options) + ']'
    if arrow:
        retval += '{->}'
    if not start:
        start = vector(0,0,0,0)
    retval += start.str() + vect.str()
    return retval

def dot(vect,color='black'):
    retval = '\\pstThreeDDot'
    options = [ ]
    if color:
        options += [ 'linecolor='+color ]
    retval += '[' + ','.join(options) + ']'
    retval += vect.str()
    return retval

def put(vect,content):
    retval = '\\pstThreeDPut'
    retval += vect.str()
    retval += '{' + content + '}'
    return retval

def setCoor(alpha,beta):
    return '\\psset{Alpha='+str(alpha)+',Beta='+str(beta)+'}'
