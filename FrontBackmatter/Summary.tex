\pdfbookmark[1]{Summary}{Summary}

\begingroup
\let\clearpage\relax
\let\cleardoublepage\relax
\let\cleardoublepage\relax
\addcontentsline{toc}{chapter}{\tocEntry{Summary}}
\chapter*{Summary}

\emph{Why does our universe contain primarily matter, 
and hardly any anti-matter?} This question of baryogenesis is the main 
motivation for the analyses described in this thesis. \emph{New physics}
with a substantial contribution to \textsc{cp}-violating processes can
explain the baryon asymmetry. In this thesis, \emph{\myTitle},
 two analyses are presented that
search for such new physics processes: a direct search for an excited
$b$ quark that realizes additional \textsc{cp}-violation, and a 
model-independent search for \textsc{cp}-violation in the top quark production
and decay vertex.

The analyses use data recorded with the \atlas detector,
which is one of the largest physics experiments ever built.
Situated at one of the interaction points of the Large Hadron Collider 
at~\cern, Geneva, it is designed to measure the products of billions of
proton-proton
collisions a second, in search of the rare collisions where something special
happens. The creation of a top quark is an example such a rare occasion.
While pairs of a top quark and an antitop quark
are produced relatively often, single top quarks are produced on average
less than once a second.
Isolating these rare events is done with
numerous requirements on the properties of the events.
%, and involves
%the reconstruction of the top quark momentum.

The top quark was discovered two decades ago, and its mass, production cross
section and decay modes have been studied extensively.
Nevertheless, the top quark remains a very interesting research subject
due to its very high mass and the potential to find new physics in processes
involving the top quark.

%This thesis, \emph{\myTitle}, describes two analyses performed with data of
%the~\atlas detector. 
Both analyses described in this thesis focus at single top processes, processes
where a single top quark is produced, and look for new physics, or
physics \emph{beyond the Standard Model}.


\section*{Excited quarks}
\begin{wrapfigure}{r}{0.5\textwidth}
  \vspace{-20pt}
  \begin{center}
    \includegraphics[width=0.35\textwidth]{figures/bpr}
  \end{center}
  \vspace{-20pt}
  \caption{Feynman diagram of~$b^*$ production and decay.}
  \label{fig:sumbstar}
  \vspace{-10pt}
\end{wrapfigure}
The first analysis is a search for an excited bottom quark ($b^*$) which
decays to a~$W$ boson and a top quark, providing a signature similar
to that of~$Wt$-channel single top. The~$b^*$ is produced through 
chromomagnetic fusion
of a bottom quark and a gluon, which provides a relatively high cross section.
A Feynman diagram of the process is shown in figure~\ref{fig:sumbstar}.

The analysis is performed on~$4.7\unit{fb}^{-1}$ 
of~$\sqrt{s} = 7\unit{TeV}/c^2$ data collected by~\atlas in 2011,
and focuses on the single lepton channel. A stringent selection is used
to reduce the number of background events to roughly \num{30000}. 
The total invariant mass of the
events is used to discriminate signal from background. A template fitting
method is then used to set limits on the strength of the~$b^*$ couplings as a 
function of its mass.

The results are combined with the results of a similar search in the dilepton
channel, yielding a~$95\%$ credibility level exclusion of~$b^*$ quarks with
masses below~$m_{b^*} = 870\unit{GeV}/c^2$ for unit size chromomagnetic
and \standardmodel-like electroweak~$b^*$ couplings. The limits
have also been calculated as a function of the coupling strengths.


\section*{$t$-channel and \textsc{cp}-violation}
\begin{wrapfigure}{r}{0.5\textwidth}
  \vspace{-20pt}
  \begin{center}
    \includegraphics[width=0.35\textwidth]{figures/tchB}
  \end{center}
  \vspace{-20pt}
  \caption{Feynman diagram of single top \mbox{$t$-channel} production.}
  \label{sum:tchan}
  \vspace{-10pt}
\end{wrapfigure}
The second analysis focuses on the~$t$-channel, of which a Feynman
diagram is shown in figure~\ref{sum:tchan}. At the \lhc, 
        this channel has the highest cross
section of all single top channels, and is especially interesting because the
produced top quark is polarized. 

This analysis 
uses~$20.28\unit{fb}^{-1}$ of data with~$\sqrt{s} = 8\unit{TeV}/c^2$, collected
by~\atlas in 2012. First a selection is made to isolate
leptonic~$t$-channel events, in which the~$W$ boson decays to an electron or
a muon. This selection results in a signal-to-background ratio 
of~$S/B = 1$, 
  and selects about~\num{3500} signal
  events out of roughly~2 million produced in 2012.
Using this selection, a cut and count cross section measurement is performed,
yielding a cross section 
of~$\sigma_{t\mathrm{-channel}} = 100.1\pm2.22\mathrm{(stat)}^{+13.3}_{-13.3}\mathrm{(sys)}^{+19.1}_{-19.1}\mathrm{(th)}\unit{pb}$.

%$\sigma_{t\mathrm{-channel}} = 101.9 \pm 2.8\mathrm{(stat)}^{+10.0}_{-10.2}\mathrm{(sys)}^{+4.7}_{-3.7}\mathrm{(th)}\unit{pb}$.
%$\sigma_{t\mathrm{-channel}} = 100.1^{+23.4}_{-23.0}\unit{pb}$.
The uncertainty of this measurement is dominated by the~\montecarlo generator
uncertainty.

\begin{wrapfigure}{l}{0.5\textwidth}
  \vspace{-20pt}
  \begin{center}
    \includegraphics[width=0.35\textwidth]{figures/thetaLN}
  \end{center}
  \vspace{-20pt}
  \caption{Angle between the momentum of the lepton in the~$W$ boson
  rest frame and the normal axis, as defined in the top quark rest frame.}
  \label{sum:normal}
  \vspace{-10pt}
\end{wrapfigure}
In the second part of the analysis, the rest frames of the~$W$ boson and the top
quark are reconstructed. By calculating the angle between 
the lepton and the light quark ($q'$) in the top rest frame, 
    the polarization of the top
quark can be measured with a folding method and likelihood fit, resulting
in~$P = 0.924 \pm 0.068\mathrm{(stat)}^{+0.178}_{-0.191}\mathrm{(sys)}^{+0.106}_{-0.122}\mathrm{(th)}$.
%$P = 0.924^{+21.8}_{-23.7}$.
The fit simultaneously
determines the~$t$-channel
cross section, resulting in a measurement comparable to the one described
above:~$\sigma_{t\mathrm{-channel}} = 101.9 \pm 2.8\mathrm{(stat)}^{+10.0}_{-10.2}\mathrm{(sys)}^{+4.7}_{-3.7}\mathrm{(th)}\unit{pb}$
%$\sigma_{t\mathrm{-channel}} = 101.9 ^{+23.5}_{-23.6}\unit{pb}$.
Also in this measurement the uncertainty is dominated by 
the~\montecarlo generator uncertainty.
Also the angle of the lepton in the normal frame,
which is defined in figure~\ref{sum:normal}, is calculated. This is then
used to measure the forward-backward asymmetry in the normal 
frame:~$\afbn = -0.018 \pm 0.044\mathrm{(stat)}^{+0.019}_{-0.018}\mathrm{(sys)}^{+0.005}_{-0.005}\mathrm{(th)}$.
%$\afbn = -0.018\pm0.048$. 
All measurements are consistent with 
the~Standard Model.

The measurements of the top quark polarization and the forward-backward
asymmetry in the normal frame are then combined to a measurement of the
imaginary part of the right-handed tensor coupling ($\Im(g_R)$), which
is sensitive to \textsc{cp}-violation.
The result
is~$\Im(g_R) = -0.030 \pm 0.074\mathrm{(stat)}^{+0.025}_{-0.023}\mathrm{(sys)}^{+0.008}_{-0.008}\mathrm{(th)}$,
%$\Im(g_R) = -0.030 ^{+0.079}_{-0.078}$,
which is also fully compatible with the Standard Model.
Therefore no \textsc{cp}-violation is observed. 
%However,
%the $Wtb$ vertex can still provide a significant source of 
%\textsc{cp}-violation.

In conclusion, neither analysis described in this thesis has found any
deviation from the Standard Model. However, the $Wtb$ vertex can still
provide a significant source of \textsc{cp}-violation and the hypothesis
for the baryogenesis is not yet ruled out. It may well be that future
\lhc data provides the answer to this big question.



\newpage
\selectlanguage{dutch}
\sisetup{output-decimal-marker={,}}
\addcontentsline{toc}{chapter}{\tocEntry{Samenvatting}}
\chapter*{Samenvatting}
\emph{Waarom bevat ons universum voornamelijk materie, en nauwelijks
    antimaterie?} Deze kwestie van baryogenesis is de belangrijkste
    motivatie voor de analyses die dit proefschrift zijn bescreven.
\emph{Nieuwe fysica} met een substanti\"ele contributie aan 
\textsc{lp}-schendende processen zou de baryon-asymmetrie kunnen verklaren.
Dit proefschrift, \emph{Searches for new physics through single top},
presenteerd twee analyses die zoeken naar zulke nieuwe fysica: een directe
zoektocht naar een aangeslagen $b$-quark, en een modelonafhankelijke
zoektocht naar \textsc{lp}-schending in de productie- en vervalsvertex van
het top-quark.

De analyses gebruiken data die opgenomen zijn met het \textsc{atlas}-detector,
dat een van de grootste natuurkundige experimenten is ooit 
gebouwd is. Het is gelegen aan een van de interactiepunten van de 
Large Hadron Collider
 bij \textsc{cern}, Gen\'eve, en ontworpen om miljarden proton-proton 
botsingen per seconde te meten, op zoek naar de zeldzame botsingen 
waar iets bijzonders gebeurt. De productie van een top-quark is een 
voorbeeld van zo'n zeldzame gelegenheid. 
Hoewel paren van een top-quark en een antitop-quark relatief vaak voorkomen,
worden enkele top-quarks gemiddeld minder
dan \'e\'en keer per seconde geproduceerd. 
Het isoleren van deze zeldzame gebeurtenissen wordt gedaan door tal van eisen
te stellen aan de eigenschappen van de gebeurtenissen.

Het top quark werd twee decennia geleden ontdekt, en zijn massa, 
productiedoorsnede en vervalmodi zijn uitgebreid bestudeerd. 
Toch blijft het top-quark
een zeer interessant onderzoeksobject vanwege de zeer hoge massa en de 
potentie om nieuwe fysica vinden in  de processen van het top-quark.

%Dit proefschrift, \emph{Searches for new physics through single top}, 
%beschrijft twee analyses van de gegevens van de ATLAS-detector. 
Beide analyses in dit proefschrift zijn gericht op enkele-topprocessen,
processen waarbij een enkel top-quark wordt geproduceerd, 
en zoeken naar nieuwe fysica, of \emph{natuurkunde voorbij het Standaardmodel}.

\section*{Aangeslagen quarks}
\begin{wrapfigure}{r}{0.4\textwidth}
  \vspace{-20pt}
  \begin{center}
    \includegraphics[width=0.35\textwidth]{figures/bpr}
  \end{center}
  \vspace{-20pt}
  \caption{Feynmandiagram van~$b^*$-productie en -verval.}
  \label{fig:sumbstarNL}
  \vspace{-10pt}
\end{wrapfigure}
De eerste analyse is een zoektocht naar een aangeslagen bottom-quark ($b^*$)
dat vervalt naar een~$W$-boson en een top-quark, zodat een signaal
 vergelijkbaar aan dat van het~$Wt$-kanaal enkele-top ontstaat. De~$b^*$ 
wordt geproduceerd door middel van chromomagnetische fusie van een bottom-quark
en een gluon, wat een relatief
hoge productiedoorsnede mogelijk maakt. Een Feynmandiagram van
het proces is weergegeven in figuur~\ref{fig:sumbstarNL}.

De analyse is uitgevoerd op~$\num{4.7}\unit{fb}^{-1}$ 
$\sqrt{s} = 7\unit{TeV}/c^2$ door \atlas in 2011 verzamelde data, en
richt zich op het enkele-leptonkanaal. Een strenge
selectie wordt gebruikt om het aantal achtergrondgebeurtenissen te reduceren 
tot ongeveer $\num{30000}$. De
totale invariante massa van de gebeurtenissen wordt gebruikt om het signaal 
te onderscheiden van de achtergrond. Een
sjabloon-fittingmethode wordt vervolgens gebruikt om limieten op de
sterkte van de $b^*$-koppelingen te zetten
als een functie van de massa.

De resultaten zijn gecombineerd met de resultaten van een soortgelijke
analyse  in het dileptonkanaal,
wat een exclusie oplevert van~$b^*$-quarks met massa's 
onder~$m_{b^*} = 870\unit{GeV}/c^2$ met een geloofwaardigheidsniveau van~$95\%$
voor chromomagnetische koppelingen met sterkte~$1$ en Standaardmodel-achtige
elektrozwakke $b^*$-koppelingen.
De limieten zijn eveneens berekend als functie van de koppelingssterkten.

\section*{$t$-kanaal and \textsc{lp}-schending}
\begin{wrapfigure}{r}{0.4\textwidth}
  \vspace{-20pt}
  \begin{center}
    \includegraphics[width=0.35\textwidth]{figures/tchB}
  \end{center}
  \vspace{-20pt}
  \caption{Feynmandiagram van enkele-top \mbox{$t$-kanaal} productie.}
  \label{sum:tchanNL}
  \vspace{-10pt}
\end{wrapfigure}

De tweede analyse richt zich op het~$t$-kanaal,
waarvan een feynmandiagram is weergegeven in figuur~\ref{sum:tchanNL}.
Bij de \lhc  heeft dit kanaal 
de hoogste productiedoorsnede van alle enkele-top-kanalen.
Het is vooral interessant omdat het geproduceerde top-quark gepolariseerd is.

Deze analyse maakt gebruik van~$\num{20.28}\unit{fb}^{-1}$ aan data met 
$\sqrt{s} = 8\unit{TeV}/c^2$ die door \atlas is verzameld in 2012.
Eerst wordt een
selectie gemaakt om leptonische~$t$-kanaalgebeurtenissen, 
waarbij het W-boson vervalt tot een elektron of een muon,
te isoleren.
Deze selectie resulteert in een signaal-
tot-achtergrond verhouding van $S/B = 1$, en selecteert ongeveer $\num{3500} $
signaalgebeurtenissen uit de ongeveer 2
miljoen geproduceerde in 2012. Met deze selectie wordt een snij-en-tel
doorsnedemeting uitgevoerd, waarmee een doorsnede van~$\sigma_{t\mathrm{-kanaal}} = \num{100.1}^{+\num{23.4}}_{-\num{23.0}}\unit{pb}$ wordt gevonden.
De
onzekerheid van deze meting wordt gedomineerd door de 
montecarlogenerator-onzekerheid.

\begin{wrapfigure}{l}{0.4\textwidth}
  \vspace{-20pt}
  \begin{center}
    \includegraphics[width=0.35\textwidth]{figures/thetaLN}
  \end{center}
  \vspace{-20pt}
  \caption{Hoek tussen de impuls van het lepton in 
het~$W$-boson-ruststelsel en de loodrechte as, zoals gedefinieerd in het
top-quark-ruststelsel.}
  \label{sum:normalNL}
  \vspace{-10pt}
\end{wrapfigure}

In het tweede deel van de analyse, worden de ruststelsels van 
het~$W$-boson en het top-quark gereconstrueerd.
Door het berekenen van de hoek tussen
het lepton en het lichte-quark ($q'$) in het top-ruststelsel,
kan de polarisatie van het top-quark worden
gemeten met een vouwmethode en waarschijnlijkheidsfit,
wat resulteert in~$P = \num{0.924}^{+\num{21.8}}_{-\num{23.7}}$.
 De fit bepaalt gelijktijdig
de~$t$-kanaal-doorsnede,
wat resulteert in een meting te vergelijken met de meting die
hierboven is beschreven: 
$\sigma_{t\mathrm{-kanaal}} = \num{101.9}^{+\num{23.5}}_{-\num{23.6}}\unit{pb}$.
In deze meting wordt de onzekerheid ook gedomineerd door de
montecarlogenerator-onzekerheid.
Ook de hoek van het lepton in het normale stelsel, zoals gedefini\"eerd
in figuur~\ref{sum:normalNL}, wordt berekend. Dit wordt daarna gebruikt om
de voorwaarts-achterwaarts\-asymmetrie in het normale stelsel te meten:
$\afbn = -\num{0.018}\pm\num{0.044}(\mathrm{stat})^{+\num{0.019}}_{-\num{0.018}}(\mathrm{sys})^{+\num{0.005}}_{-\num{0.005}}(\mathrm{th})$. Alle resultaten zijn
consistent met het Standaardmodel.

The metingen van de polarisatie van het top-quark en de 
voorwaarts-achterwaarts\-asymmetrie in het normale stelsel worden daarna
gecombineerd tot een meting van het imaginaire deel van de rechtshandige
tensorkoppeling ($\Im(g_R)$), die gevoelig is voor \textsc{lp}-schending.
Het resultaat is $\Im(g_R) = -\num{0.030}\pm\num{0.074}(\mathrm{stat})^{+\num{0.025}}_{-\num{0.023}}(\mathrm{sys})^{+\num{0.008}}{-\num{0.008}}(\mathrm{th})$,
    wat ook volledig compatibel is met het Standaardmodel. Geen 
    \textsc{lp}-schending is waargenomen. 

In conclusie: geen van beide analyses die beschreven zijn in dit proefschrift
heeft enige afwijking gevonden van het Standaardmodel. Desalniettemin kan
de~$Wtb$-vertex een belangrijke bron van \textsc{lp}-schending zijn, en de
hypothese van baryogenesis is nog niet verworpen. Toekomstige data van de
\lhc kan het antwoord op deze vraag geven.

\newpage
\addcontentsline{toc}{chapter}{\tocEntry{Populair-wetenschappelijke introductie}}
\chapter*{Populair-wetenschappelijke introductie}
In dit proefschrift, \emph{\myTitle}, beschrijf ik twee analyses die ik heb
gedaan met de data van de~\atlas-detector, een van de
grootste deeltjesfysica-experimenten ooit gedaan.
In deze samenvatting geef ik een toegankelijke inleiding in de deeltjesfysica
en sluit ik af met een beschrijving van het onderzoek in dit proefschrift.

\section*{Elementaire deeltjes}
De deeltjesfysica draait om de vraag \emph{waar is de wereld van gemaakt?}
De wereld om ons heen bevat veel verschillende dingen zoals mensen, 
dieren en planten, maar ook levenloze dingen als zand, lucht
en dit proefschrift. De vraag waar al die dingen van gemaakt zijn houdt
de mensheid al enkele millenia bezig. 

Zo dacht de Siciliaanse filosoof
Empedocles, zo'n 450 jaar voor het begin van de jaartelling, dat alles
in de wereld was gemaakt van vier elementen: \emph{vuur}, \emph{aarde},
\emph{lucht} en \emph{water}. Aristoteles bedacht daar ongeveer een
eeuw later een vijfde element
bij: \emph{ether}, waar de hemel en de goden van gemaakt zouden moeten zijn.

Veel andere culturen in de klassieke periode,
     zoals de Egyptenaren en de Buddhisten,
hadden dezelfde vier of vijf elementen. Enkele culturen hadden hier varianten
op, zoals de Tibetanen (\emph{ruimte} in plaats van \emph{ether}) 
    en de Chinezen (\emph{vuur}, \emph{aarde}, \emph{metaal},
    \emph{water} en \emph{hout}). 

In ruwweg dezelfde periode van de Griekse oudheid filosofeerde Democritos dat
alle materie is opgebouwd uit piepkleine deeltjes, die niet verder op te delen
zijn in kleinere stukjes. Hij noemde deze elementaire deeltjes \emph{atomen},
     wat `ondeelbaar' betekent.

\smallskip
Het heeft vele eeuwen geduurd voor er verandering kwam in de klassieke idee\"en
over de
elementen: in de eerste helft van de $18^\mathrm{e}$ eeuw werden enkele
chemische elementen gevonden. De Fransman Antoine-Laurent de Lavoisier
publiceerde in~1789 een lijst~\cite{lavoisier} met daarin rond de dertig
`chemische elementen', waaronder zuurstof en waterstof, maar ook licht.

Het aantal elementen groeide in de loop der jaren, en ruwweg een eeuw later
zag Dmitrii Mendeleev het licht~\cite{mendeleev}: hij ontdekte een systeem 
(het \emph{periodiek systeem}) in de eigenschappen
van de (op dat moment) 64 elementen. Met dat systeem kon hij
het bestaan van nog niet ontdekte elementen, en hun eigenschappen, voorspellen. 

In dezelfde periode kwam de Engelsman John Dalton met de eerste waarnemingen
die de atoomtheorie, de theorie dat alle materie uit atomen bestaat, steunden. 
In~1905 kwam Albert Einstein met berekeningen die de theorie 
steunden~\cite{brownian}, en Jean Baptiste Perrin kwam met de beslissende
waarnemingen: het stond vast dat materie uit atomen bestond.

\smallskip
De theorie over de materie bleek
echter nog niet compleet. Sir Joseph John Thomson
ontdekte een deeltje dat veel
kleiner was dan een atoom: het elektron~\cite{thomson}. Het feit dat elektronen
zich \emph{in} atomen bevinden betekende dat atomen, ondanks hun naam,
     niet ondeelbaar zijn. Enkele decennia later ontdekte Ernest Rutherford
de atoomkern, waarvan hij niet veel later aantoonde dat deze uit protonen
bestaat~\cite{rutherford}.

\begin{wrapfigure}{r}{0.4\textwidth}
  \vspace{-20pt}
  \begin{center}
    \includegraphics[width=0.35\textwidth]{sumplots/atom}
  \end{center}
  \vspace{-20pt}
  \caption{Het klassieke atoommodel, met in het middel een kern met protonen
      en neutronen (rood en blauw) en daaromheen elektronen in 
          cirkelbanen (zwart).}
  \label{fig:atoom}
  \vspace{-10pt}
\end{wrapfigure}
Daarmee was de theorie van de materie plots heel anders: het aantal elementaire
deeltjes was teruggebracht van vele tientallen naar slechts drie (het elektron,
het proton en het foton, dat in 1896 was ontdekt door 
Wilhelm R\"ontgen~\cite{roentgen}). Men wist nu dat atomen samengestelde
deeltjes waren, bestaande uit een atoomkern van protonen en daaromheen 
elektronen. 
Het bleef echter niet bij de drie elementen: James Chadwick toonde
aan dat er naast protonen ook neutronen in atoomkernen 
zitten~\cite{1932Natur.129Q.312C}. Zo kwam het model van een atoom er zo 
uit te zien als in figuur~\ref{fig:atoom}.

Ook hierbij bleef het niet: het muon, een zwaardere, instabiele variant van
het elektron, werd ontdekt in~1937~\cite{1937PhRv...51..884N}. Tien jaar later
werden kort na elkaar het pi-meson (pion), het K-meson (kaon) en 
het~$\Lambda^0$-baryon ontdekt. Daarna ging het snel: binnen vijftien jaar
werden twee neutrino's, extreem lichte deeltjes die vrijwel overal ongehinderd
doorheen vliegen, en enkele tientallen nieuwe mesonen en baryonen 
ontdekt.

\smallskip
De grote hoeveelheid mesonen en baryonen deed vermoeden dat ook deze deeltjes,
net als de atomen, geen elementaire deeltjes waren. 
Dat bleek in 1969, toen een structuur in het proton (een baryon) werd 
gemeten~\cite{1969PhRvL..23..930B,1969PhRvL..23..935B}.
De deeltjes in het proton
werden al snel ge\"identificeerd als 
quarks~\cite{1964PhL.....8..214G,Zweig:352337}. Wederom was de theorie van
de materie plots heel anders. Er waren nu acht elementaire deeltjes:
het foton, twee \emph{leptonen} (het elektron en het muon), twee
neutrino's en drie quarks. Alle mesonen en baryonen bestaan uit 
combinaties van de drie quarks, het up-quark,  down-quark en het strange-quark,
zoals het proton en het neutron in figuur~\ref{fig:partons}.
\begin{figure}[hbt]
  \centering
  \includegraphics[width=\textwidth]{sumplots/partons}
  \caption{De substructuur van het proton en neutron: het proton bestaat
      uit twee up-quarks en een down-quark, en het neutron bestaat uit
          twee down-quarks en een up-quark.}
  \label{fig:partons}
\end{figure}

Kort daarop werd het aantal elementaire deeltjes echter weer uitgebreid
met de ontdekking van een derde lepton 
(het tau-lepton)~\cite{1975PhRvL..35.1489P}, en
een vierde en een vijfde quark 
(het charm-quark en het bottom-quark)~\cite{1974PhRvL..33.1404A,1974PhRvL..33.1406A,1977PhRvL..39..252H}. Het gluon, het krachtdeeltje dat quarks bijeen
houdt in bijvoorbeeld protonen, werd ontdekt in~1979~\cite{1979PhRvL..43..830B},
en de W- en Z-bosonen die verantwoordelijk zijn voor radioactief $\beta$-verval
van atoomkernen niet lang daarna in~1983~\cite{1983PhLB..123..275A,1983PhLB..126..398A}. 
Daarna volgden nog het top-quark in~1995~\cite{PhysRevLett.74.2632,PhysRevLett.74.2626}, het tau-neutrino
in~2000~\cite{2001PhLB..504..218D}, en meest recentelijk het Higgs-boson
in~2012~\cite{Aad:2012tfa,Chatrchyan:2012ufa}.

\smallskip
\begin{figure}[htb]
  \centering
  \includegraphics[width=\textwidth]{sumplots/sm}
  \caption{Alle deeltjes in het Standaardmodel. }
  \label{fig:sumsm}
\end{figure}
Met die laatste ontdekking is het \emph{Standaardmodel}, het model dat
alle deeltjes en interacties beschrijft, compleet. We kennen nu 17 
deeltjes\footnote{Hierbij tel ik de antideeltjes, deeltjes met vrijwel dezelfde
maar soms omgekeerd eigenschappen, niet mee.}, die opgedeeld kunnen worden
in de \emph{fermionen}, de materiedeeltjes, en de \emph{bosonen}, de 
krachtdeeltjes (zie ook figuur~\ref{fig:sumsm}). De materiedeeltjes kunnen
weer opgedeeld worden in de \emph{quarks} en de \emph{leptonen}, waarvan
er elk drie families bestaan. 

Alle materie in onze wereld is gemaakt van
de deeltjes in de eerste familie: protonen en neutronen bestaan uit up- en
down-quarks, en atomen bestaan uit een kern van protonen en neutronen en
daaromheen elektronen. De deeltjes in de hogere families zijn vrijwel identiek
aan hun buren in de eerste familie, maar zijn zwaarder en daarom 
\emph{instabiel}: ze vervallen na een (zeer) korte tijd in lichtere deeltjes.
De neutrino's zijn een apart verhaal, aangezien ze alledrie zeer licht
zijn en vrijwel niet te detecteren zijn: die kom je in het dagelijks leven
niet tegen.

De bosonen zijn geen materie, maar brengen krachten over. Zo wordt
de elektromagnetische kracht (licht) overgebracht door fotonen.
Het gluon brengt de sterke kernkracht over, en de W- en Z-bosonen zijn
de overbrengers van de zwakke kernkracht. Het Higgs-boson brengt geen
kracht over, maar zorgt er voor dat deeltjes massa hebben: als het niet
zou bestaan zou alles altijd met de lichtsnelheid bewegen.

\smallskip
Het feit dat het Standaardmodel nu compleet is betekent niet dat we achterover
kunnen gaan zitten. Het model heeft namelijk nog enkele tekortkomingen.
Zo komt de vierde kracht die we kennen, de zwaartekracht, niet voor in het
Standaardmodel. Het graviton, het deeltje dat de zwaartekracht over zou
brengen, past namelijk niet in de theorie.
Een ander probleem is dat
het Standaardmodel het bestaan van donkere materie, massa die in
sterrenstelsels aanwezig is maar geen licht uitstraalt, niet kan verklaren.

\begin{figure}[htb]
  \centering
  \includegraphics[width=\textwidth]{sumplots/nparts}
  \caption{Het aantal `elementaire' deeltjes door de eeuwen heen.}
  \label{fig:nparts}
\end{figure}

Figuur~\ref{fig:nparts} vat  de geschiedenis van de deeltjesfysica kort
samen. Weergegeven is het aantal deeltjes waarvan men dacht dat ze
elementair waren als functie van de tijd. Al tweemaal in de afgelopen
eeuwen is gebleken dat deeltjes, waarvan men dacht dat ze elementair waren,
samengesteld waren uit andere deeltjes. Al is  het natuurlijk verleidelijk
om te denken dat de deeltjes in Standaardmodel allemaal elementair zijn,
gezien de geschiedenis zou dat onverstandig zijn. Op dit moment weten we echter
niet beter.

Ook daarom wordt er druk gezocht naar zogenaamde \emph{nieuwe fysica} of
afwijkingen van het Standaardmodel.
Er zijn voldoende theorie\"en die
voorspellingen doen over wat er gevonden zal worden, zoals
supersymmetrie~\cite{Dawson:1996cq} en stringtheorie~\cite{Tong:2009np},
maar feit is dat we niet weten wat we zullen vinden. Misschien een 
supersymmetrisch deeltje? Of een vier generatie materie? Een substructuur
in het elektron of een quark? We weten het niet. Het zou natuurlijk ook goed
kunnen dat we iets vinden dat we totaal niet verwachten.

\section*{Versnellers en detectoren}
Het onderzoek beschreven in dit proefschrift gaat over de allerkleinste
deeltjes die bestaan. Om het onderzoek te doen, en deze deeltjes waar te
nemen, zijn gigantisch grote apparaten nodig. Waarom kunnen we deze deeltjes
niet `gewoon' zien?

Zien doen we normaliter met onze ogen, maar daarmee kunnen we geen
dingen onderscheiden die kleiner zijn dan ruwweg~$\num{0.1}\unit{mm}$.
Ter vergelijking: een proton is ongeveer~$\num{1}\unit{fm}$ 
breed\footnote{Een femtometer (fm) is $10^{-15}\unit{m}$,
ofwel $\num{0.000000000000001}\unit{m}$.}. Met een microscoop kunnen we veel
kleinere objecten onderscheiden, maar zelfs met de beste microscoop is het
nooit mogelijk om kleinere objecten dan zo'n~$500\unit{nm}$ te zien\footnote{Een
nanometer (nm) is $10^{-9}\unit{m}$, ofwel $\num{0.000000001}\unit{m}$.}.
Dat komt niet door de microscoop, maar door de kwantummechanische eigenschappen
van het licht waarmee we objecten 
bekijken: de fotonen in zichtbaar licht hebben een golflengte, die je
kort door de bocht kan opvatten als de breedte van het deeltje,
tussen
de~$400$ en~$700\unit{nm}$, en zijn dus (veel) groter dan een proton.
Het waarnemen van een proton met (zichtbaar) licht is net als het lezen
van braille met een skippybal of het afspelen van een langspeelplaat met een
wolkenkrabber als naald: dat werkt niet.
  
Alle elementaire deeltjes hebben een dergelijke golflengte, die afhangt
van de energie van het deeltje\footnote{Dit is de Comptiongolflengte, die
gegeven wordt door~$\lambda = \frac{hc}{E}$, waar~$E$ de energie is van het
deeltje en~$h$ en~$c$ constanten zijn.}: hoe groter de energie, hoe kleiner de 
golflengte. Deeltjes die klein genoeg zijn om een atoomkern te bekijken moeten
veel energie hebben. Om in een proton te kijken is gigantisch veel energie
nodig. Om al die energie in een deeltje te krijgen moet het deeltje versneld
worden tot bijna de lichtsnelheid.

\smallskip
Om de deeltjes tot zo'n grote snelheid te versnellen is een hele grote machine
nodig. De Large Hadron Collider (\lhc) 
bij \textsc{cern}, Gen\`eve, is zo'n grote
machine. De \lhc ligt 100 meter ondergronds, in een tunnel die een rondje
van 27 kilometer beschrijft. In de versneller worden protonen versneld tot
bijna de lichtsnelheid. Op maximale snelheid vliegen de protonen meer
dan $\num{11000}$ rondjes per seconde, verdeeld in twee bundels: de ene
linksom, de andere rechtsom.

Op vier plekken in de versneller, de zogenaamde interactiepunten,
 snijden de bundels elkaar. Hier botsen de
protonen met gigantische energie op elkaar, waarna fragmenten en nieuwe deeltjes
alle kanten op vliegen. Rond deze interactiepunten zijn grote detectoren
gebouwd, waarvan \atlas er \'e\'en is. \Atlas is met
zijn~$46 \times 25 \times 25\unit{meter}$ de grootste detector van de \lhc.

\begin{figure}[pht]
\includegraphics[angle=0,width=\textwidth]{figures/0803012_01.jpg}
\caption{Een overzicht van de \atlas-detector.}
\label{fig:ATLASsum}
\end{figure}




\section*{Dit proefschrift}
De analyses die ik beschrijf in dit proefschrift draaien om top-quarks.
Top-quarks zijn de zwaarste deeltjes die we kennen, met een massa meer
dan~$\num{300000}$ keer groter dan die van een elektron. Mede door die
grote massa bestaan top-quarks maar heel kort: na~$10^{-25}\unit{s}$
vervallen ze\footnote{Dat is $\num{0.0000000000000000000000001}\unit{s}$.},
vrijwel altijd in een bottom-quark en een~$W$-boson. 

In versnellers als
de~\lhc worden top-quarks meestal in paren gemaakt: een top-quark en
een antitop-quark, zijn antideeltje. Top-quarks kunnen ook all\'e\'en 
geproduceerd worden. Er zijn drie van zulke zogenaamde 
\emph{single top}-processen. Deze worden heel cryptisch het~$t$-kanaal,
    het~$s$-kanaal en het~$Wt$-kanaal genoemd. 
Ik heb twee analyses gedaan, gericht op twee verschillende single top-kanalen.
\smallskip

De eerste van de twee analyses is 
gericht op het~$Wt$-kanaal. In dit proces wordt het
top-quark samen met een $W$-boson gemaakt. In dit kanaal heb ik gezocht naar
een hypothetisch n\'og zwaarder quark, het~$b^*$-quark, dat zou vervallen in
een top-quark en een~$W$-boson. Dit zijn dezelfde deeltjes als die in
het~$Wt$-kanaal geproduceerd worden, en daarom 
zou het signaal van het~$b^*$-quark 
erg lijken op het signaal van het~$Wt$-kanaal. Omdat het~$b^*$-quark zo zwaar
zou zijn is het toch mogelijk de twee van elkaar te onderscheiden.

In deze analyse heb ik het~$b^*$-quark niet kunnen ontdekken. Desondanks kan
het nog wel bestaan, maar als het bestaat komt het \'of heel weinig voor, 
\'of het is te zwaar om in de~\lhc gemaakt te kunnen worden.
\smallskip

De tweede analyse richt zich op het~$t$-kanaal. Dit proces, waarin het top-quark
samen met een lichter quark wordt gemaakt, komt het vaakst
voor van de single top-processen. 

Allereerst heb ik de \emph{werkzame doorsnede} van dit proces gemeten: de 
werkzame doorsnede is een maat voor hoe vaak het proces voorkomt. Om deze
doorsnede te bepalen is het nodig om dit proces heel goed te kunnen 
onderscheiden van processen die erop lijken: de zogenaamde achtergronden.

Eenmaal ontdaan van de achtergronden is het ook mogelijk eigenschappen
van het proces te bestuderen. E\'en van de interessante eigenschappen van
het~$t$-kanaal-proces is de polarisatie van het top-quark: de manier waarop
het top-quark om zijn as draait. Aan de hand van deze polarisatie kunnen 
we vaststellen welke koppelingen in het proces een rol spelen. 
Ook in deze analyse heb ik geen afwijkingen van het Standaardmodel gevonden.
\smallskip


\endgroup
