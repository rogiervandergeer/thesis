#!/usr/bin/env python2
import matplotlib
matplotlib.use('Agg')
from matplotlib import pyplot as plt
from scipy.interpolate import interp1d
import numpy as np
import math

plt.xkcd()

fig = plt.figure()

def part(name,letter,color,x,y,vAlign=0.0,hAlign=0.0):
    fig.gca().add_artist(plt.Circle((x,y),0.35,fill=True,color=color))
    fig.gca().text(x-hAlign,y-vAlign,letter,color='w',fontsize=36,
            horizontalalignment='center',verticalalignment='center')
    fs = 8
    if len(name) > 16: fs = 6
    fig.gca().text(x,y-0.5,name,color='k',fontsize=fs,
            horizontalalignment='center',verticalalignment='center')

part('up-quark',r'$u$','g',0.0,0)
part('charm-quark',r'$c$','g',1.0,0)
part('top-quark',r'$t$','g',2.0,0)
part('down-quark',r'$d$','g',0.0,-1)
part('strange-quark',r'$s$','g',1.0,-1)
part('bottom-quark',r'$b$','g',2.0,-1)

part('elektron',r'$e$','r',0.0,-2.2)
part('muon',r'$\mu$','r',1.0,-2.2)
part('tauon',r'$\tau$','r',2.0,-2.2)
part('elektronneutrino',r'$\nu_e$','r',0.0,-3.2,vAlign=0.03)
part('muonneutrino',r'$\nu_\mu$','r',1.0,-3.2,vAlign=0.07,hAlign=-.02)
part('tau-neutrino',r'$\nu_\tau$','r',2.0,-3.2,vAlign=0.03)

part('foton',r'$\gamma$','b',3.2,0)
part('gluon',r'$g$','b',4.2,0)
part('W-boson',r'$W$','b',3.2,-1,vAlign=0.05)
part('Z-boson',r'$Z$','b',4.2,-1,vAlign=0.05)
part('Higgs-boson',r'$H$','b',3.7,-2.7,vAlign=0.05)

for i in range(3):
    fig.gca().text(i,0.5,'I'*(i+1),color='k',fontsize=18,
            horizontalalignment='center',verticalalignment='center')

fig.gca().text(1,1,'fermionen',color='k',fontsize=24,
        horizontalalignment='center',verticalalignment='center')
fig.gca().text(3.7,1,'bosonen',color='k',fontsize=24,
        horizontalalignment='center',verticalalignment='center')
fig.gca().text(-0.7,-0.5,'quarks',color='k',fontsize=18,rotation=90,
        horizontalalignment='center',verticalalignment='center')
fig.gca().text(-0.7,-2.7,'leptonen',color='k',fontsize=18,rotation=90,
        horizontalalignment='center',verticalalignment='center')

plt.plot([2.6,2.6],[0.5,-3.7],'k-',lw=2)
plt.plot([-0.5,2.5],[-1.7,-1.7],'k-',lw=1)

fig.gca().axis('equal')
fig.gca().axis('off')
fig.gca().set_ylim([-4,1.1])
fig.gca().set_xlim([-1,5])
plt.savefig('sm.pdf')
