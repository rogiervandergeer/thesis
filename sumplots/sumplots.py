#!/usr/bin/env python2
import matplotlib
matplotlib.use('Agg')
from matplotlib import pyplot as plt
from scipy.interpolate import interp1d
import numpy as np
import math

def p(x):
    if x != None: 
        X = 2050-x
        return -math.log(X)
    else: return None
def r(x):
    return math.log(x)

plt.xkcd()

fig = plt.figure()
ax = fig.add_subplot(1, 1, 1)
ax.spines['right'].set_color('none')
ax.spines['top'].set_color('none')
ax.xaxis.set_ticks_position('bottom')
ax.yaxis.set_ticks_position('left')
xt = [ 1600,1800,1900,1950,1980,2000, 2015 ]
plt.xticks([p(1200)]+[p(x) for x in xt],['oudheid']+xt)
yt = [ 2, 4, 10, 20, 50, 100 ]
plt.yticks([r(y) for y in yt],yt)

#plt.annotate(
#            'THE DAY I REALIZED\nI COULD COOK BACON\nWHENEVER I WANTED',
#                xy=(70, 1), arrowprops=dict(arrowstyle='->'), xytext=(15, -10))

elmX = [ None, 1100, 1250, 1400, 1650 ]
elmY = [ 4 ] * len(elmX)

chemX = [ 1550, 1669, 1789, 1869, 1900, 1950, 1975, 2000, 2010 ]
chemY = [    9,   13,   29,   64,   82,   95,  104,  111,  115 ]

chemX = chemX[:-1]
chemY = chemY[:-1]

hadX = [ None, 1919, 1932, 1937, 1947, 1950, 1953, 1960, 1970 ]
hadY = [    1,    3,    4,    6,    7,    8,   10,   20,   30 ]

partX = [ None, 1969, 1974, 1975, 1977, 1979, 1983, 1995, 2000, 2012, 2020 ]
partY = [    1,    8,    9,   10,   11,   12,   14,   15,   16,   17,   18 ]
#chemF = interp1d(chemX,chemY,kind='quadratic')
#chemL = np.linspace(min(chemX),max(chemX),100)
#plt.plot(chemL,chemF(chemL),'r')
#plt.plot([

def margin(data,f=0.18):
    retval = [ data[0] + f*(data[1]-data[0]), data[1] - f*(data[1]-data[0])]
    return retval

def plot(xdata,ydata,color,kind='quadratic'):
    xdata = [ p(x) for x in xdata ]
    ydata = [ r(x) for x in ydata ]
    func = interp1d(xdata[1:-1],ydata[1:-1],kind=kind)
    lins = np.linspace(min(xdata[1:-1]),max(xdata[1:-1]),100)
    plt.plot(lins,func(lins),color)
    if xdata[0]:
        plt.plot(margin(xdata[0:2]),margin(ydata[0:2]),color+'--')
    if xdata[-1]:
        plt.plot(margin(xdata[-2:]),margin(ydata[-2:]),color+'--')

ap = dict(facecolor='black',shrink=0.2,width=1.5,headwidth=8)
ax.set_ylim([r(2),r(120)])
plot(elmX,elmY,'m')
plot(chemX,chemY,'r')
plot(hadX,hadY,'g','linear')
plot(partX,partY,'b',kind='linear')
ax.annotate('klassieke\nelementen',xy=(p(1300),r(4)),xytext=(p(1400),r(5.6)),
    arrowprops=ap,color='r')
ax.annotate('protonen',xy=(p(1919),r(3)),xytext=(p(1750),r(3)),
    arrowprops=ap)
ax.annotate('chemische\nelementen',xy=(p(1669),r(13)),xytext=(p(1200),r(30)),
    arrowprops=ap)
ax.annotate('quarks',xy=(p(1969),r(8)),xytext=(p(1990),r(5)),
    arrowprops=ap)
ax.annotate('?',xy=(p(1969),r(8)),xytext=(p(2022),r(17)),
        verticalalignment='middle')
#plt.plot([5,10,15],(0,1,3))

plt.xlabel('tijd')
plt.ylabel('aantal "elementaire" deeltjes')
#plt.show()
plt.savefig('nparts.pdf')
plt.savefig('nparts.png')

plt.close()
from scipy.optimize import curve_fit
fig = plt.figure()
ax = fig.add_subplot(1, 1, 1)
ax.spines['right'].set_color('none')
ax.spines['top'].set_color('none')
ax.xaxis.set_ticks_position('bottom')
ax.yaxis.set_ticks_position('left')
xt = [ 1940,1960,1980,2000 ]
plt.xticks([x for x in xt],xt)
yt = [ 1,10,100,1000 ]
plt.yticks([r(y) for y in yt],yt)
ax.set_ylim([math.log(0.5),math.log(4000)])

ys = [ 1932, 1937, 1947, 1956, 1962, 1969, 1974, 1975, 
   1977, 1979, 1983, 1995, 2000, 2012 ]
at = [  1,    2,    2,    2,    7,   11,   14,   30,
     17,   60,   75,  500,   60, 3500 ]
#func = interp1d(ys, at,
#        kind='slinear')
#lins = np.linspace(min(ys),max(ys),100)
def fexp(x,a,b,c):
    return a*np.exp(b*(x-1890))+c
ys = np.array(ys)
at = np.array(at)
popt,pcov = curve_fit(fexp,ys,at)
popt[0] =0.003
popt[1] =0.105
popt[2] =0.01
#plt.plot(lins,func(lins),'b')
plt.plot(ys,[ math.log(a)  for a in fexp(ys,*popt)],'r-',label='curve')
plt.scatter(ys,[math.log(a) for a in at],s=4,color='b')
plt.xlabel('tijd')
plt.ylabel('aantal auteurs per publicatie')

def ann(text,index,offset=-20,fact=1.5):
    ax.annotate(text,xy=(ys[index],math.log(at[index])),
                xytext=(ys[index]+offset,math.log(at[index]*fact)),
    arrowprops=ap,horizontalalignment='center')

ann('Higgs-boson',-1)
ann('top-quark',-3)
ann('muon',1,offset=10,fact=10)
ann('bottom-quark',-6,offset=15,fact=0.2)
ann('tau-neutrino',-2,offset=15,fact=0.2)
ann('gluon',-5)
ann('elektronneutrino',3,offset=20,fact=1/1.5)
plt.savefig('nauth.pdf')
from matplotlib.patches import Ellipse
#plt.clf()

def coor(r,alpha,offset=None):
    if offset == None:
        return (r*math.sin(alpha),r*math.cos(alpha))
    else:
        return (r*math.sin(alpha)+offset[0],r*math.cos(alpha)+offset[1])

def vcoor(r,alpha,offset):
    return (r*math.sin(alpha)+offset[0],r*math.cos(alpha)+offset[1])

plt.close()
fig = plt.figure(figsize=(6,6))
fig.gca().add_artist(Ellipse((0,0),width=2,height=1.6,angle=35,
            fill=False,color='k',linewidth=2))
fig.gca().add_artist(Ellipse((0,0),width=2,height=1.6,angle=-55,
            fill=False,color='k',linewidth=2))
fig.gca().add_artist(plt.Circle((0.04,0.06),0.08,fill=True,color='r'))
fig.gca().add_artist(plt.Circle((-.06,0.04),0.08,fill=True,color='b'))
fig.gca().add_artist(plt.Circle((-.04,-.06),0.08,fill=True,color='r'))
fig.gca().add_artist(plt.Circle((0.06,-.04),0.08,fill=True,color='b'))
fig.gca().add_artist(plt.Circle(coor(0.94,36),0.04,fill=True,color='k'))
fig.gca().add_artist(plt.Circle(coor(0.83,145),0.04,fill=True,color='k'))
fig.gca().axis('equal')
fig.gca().axis('off')
fig.gca().set_ylim([-1.1,1.1])
fig.gca().set_xlim([-1.5,1.5])
fig.gca().get_xaxis().set_visible(False)
plt.savefig('atom.pdf')
plt.close()
fig = plt.figure(figsize=(9,4.5))
fig.gca().add_artist(plt.Circle((-1.5,0),1,fill=False,color='r',linewidth=2))
fig.gca().add_artist(plt.Circle((1.5,0),1,fill=False,color='b',linewidth=2))
c = vcoor(0.38,0,(1.5,0))
fig.gca().add_artist(plt.Circle(c,0.35,fill=True,color='k'))
fig.gca().text(c[0],c[1],r'$d$',color='w',fontsize=36,horizontalalignment='center',
        verticalalignment='center')
c = vcoor(0.38,2.09,(1.5,0))
fig.gca().add_artist(plt.Circle(c,0.35,fill=True,color='k'))
fig.gca().text(c[0],c[1],r'$u$',color='w',fontsize=36,horizontalalignment='center',
        verticalalignment='center')
c = vcoor(0.38,4.18,(1.5,0))
fig.gca().add_artist(plt.Circle(c,0.35,fill=True,color='k'))
fig.gca().text(c[0],c[1],r'$d$',color='w',fontsize=36,horizontalalignment='center',
        verticalalignment='center')
c = vcoor(0.38,0,(-1.5,0))
fig.gca().add_artist(plt.Circle(c,0.35,fill=True,color='k'))
fig.gca().text(c[0],c[1],r'$u$',color='w',fontsize=36,horizontalalignment='center',
        verticalalignment='center')
c = vcoor(0.38,4.18,(-1.5,0))
fig.gca().add_artist(plt.Circle(c,0.35,fill=True,color='k'))
fig.gca().text(c[0],c[1],r'$u$',color='w',fontsize=36,horizontalalignment='center',
        verticalalignment='center')
c = vcoor(0.38,2.09,(-1.5,0))
fig.gca().add_artist(plt.Circle(c,0.35,fill=True,color='k'))
fig.gca().text(c[0],c[1],r'$d$',color='w',fontsize=36,horizontalalignment='center',
        verticalalignment='center')
fig.gca().annotate('proton',xy=(-1.5+0.707,0.707),xytext=(0.3,1.2),color='k',
    arrowprops=ap,horizontalalignment='center',verticalalignment='center')
fig.gca().annotate('neutron',xy=(1.5-0.707,-0.707),xytext=(-.3,-1.2),color='k',
    arrowprops=ap,horizontalalignment='center',verticalalignment='center')
fig.gca().axis('equal')
fig.gca().axis('off')
fig.gca().set_ylim([-1.5,1.5])
fig.gca().set_xlim([-3,3])
plt.savefig('partons.pdf')
#fig = plt.figure()
#ax = fig.add_subplot(1, 1, 1)
#ax.bar([-0.125, 1.0-0.125], [0, 100], 0.25)
#ax.spines['right'].set_color('none')
#ax.spines['top'].set_color('none')
#ax.xaxis.set_ticks_position('bottom')
#ax.set_xticks([0, 1])
#ax.set_xlim([-0.5, 1.5])
#ax.set_ylim([0, 110])
#ax.set_xticklabels(['CONFIRMED BY\nEXPERIMENT', 'REFUTED BY\nEXPERIMENT'])
#plt.yticks([])
#
#plt.title("CLAIMS OF SUPERNATURAL POWERS")

