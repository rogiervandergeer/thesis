%************************************************
\chapter{Data, object reconstruction and simulation}\label{ch:performance}
%************************************************

\section{Performance}

The analyses described in this thesis make use of the
 proton-proton datasets recorded by \atlas 
in 2011 and 2012.
The physics conditions varied between and during these \emph{runs}. The
following subsections describe the conditions and the performance of \atlas
 during
the 2011 and 2012 proton-proton runs.

\subsection{Run conditions}
\label{sec:runcon}

The data recorded during 2011 and 2012 are treated as distinct datasets.
The analysis described in Chapter~\ref{ch:bprime} uses the dataset recorded
in 2011, for which the center-of-mass energy was $\sqrt{s}=7\unit{TeV}$.
The dataset recorded in 2012, for which the center-of-mass energy was
$\sqrt{s}=8\unit{TeV}$, is used in the analysis described 
in Chapter~\ref{ch:polarization}.

Figure~\ref{fig:lumi} shows the integrated luminosity during the 2011 and
2012 runs. The instantaneous luminosity greatly increased from the 2011 to
the 2012 run, yielding more than four times more usable data in 2012. The
total luminosity used for physics is $4.57\unit{fb}^{-1}$ for the 2011 run
and $20.3\unit{fb}^{-1}$ for the 2012 run. 

Due to the very high instantaneous luminosity delivered by the \lhc, multiple
interactions occur per bunch crossing. The number of interactions per
bunch crossing (also called \emph{pile-up}) has increased more than twofold
in the 2012 run with respect to the 2011 run. The distribution of pile-up
is shown in figure~\ref{fig:pileup}.

\begin{figure}[btp]
  \begin{center}
  \includegraphics[width=0.90\textwidth]{figures/intlumivstime2011-2012DQ.eps}
  \caption{The total integrated luminosity as a function of
      time as delivered (green),
      recorded by \atlas (yellow) and quality approved (blue) during the~2011
  and~2012 runs.}
  \label{fig:lumi}
\end{center}
\end{figure}

\begin{figure}[btp]
  \centering
  \includegraphics[width=0.85\textwidth]{figures/mu_2011_2012-dec.eps}
\caption{Luminosity-weighted distributions of the mean number of interactions
per bunch crossing (pile-up) for the~2011 (blue) and~2012 (green) runs.}
  \label{fig:pileup}
\end{figure}


\subsection{Detector performance}

As can be seen in Figure~\ref{fig:lumi}, not all delivered luminosity 
has been succesfully recorded. Table~\ref{tab:dataq} shows the fraction
of data recorded by each subsystem. With all subsystems combined, \atlas
has recorded almost 90\% of all luminosity in 2011 and over 95\% in 2012.

\begin{table}
\centering
{\footnotesize
\begin{tabular}{r | c c c | c c | c c c c | c}
 & \multicolumn{3}{c|}{Inner Tracker} & \multicolumn{2}{c|}{Calorimeters} &
  \multicolumn{4}{c|}{Muon Spectrometer} & all\\
     & pixel & \sct & \trt & \lar & tile & \msmdt & \msrpc & \mscsc & \mstgc \
                & good \\\hline
2011 & $0.998$ & $0.996$ & $0.992$ & $0.969$ & $0.992$ & $0.994$ \
	& $0.988$ & $0.994$ & $0.991$ \
    &  $0.898$ \\
2012 & $0.999$ & $0.991$ & $0.998$ & $0.991$ & $0.996$ & $0.996$ \
	& $0.998$ & $1.000$ & $0.996$  \
    &  $0.955$ \\
\end{tabular}}
\caption{Luminosity weighted fractions of good quality data recorded by the 
    \atlas subsystems and all subsystems combined for the~2011 and~2012
        proton-proton runs~\cite{twiki:dataq}.}
\label{tab:dataq}
\end{table}

Also in data which is labelled ``good'', not all detector subsystems were
fully working. In all subdetectors a small fraction of channels is inoperable,
for various reasons. The fractions of \emph{dead} channels are listed in
Table~\ref{tab:dataqweird}. Fortunately, all these fractions are very low,
and do not pose a serious problem to any analysis.

\begin{table}
  \centering
  \begin{subtable}{\textwidth}
    \centering
    \begin{tabular}{r | c c c | c c }
     & \multicolumn{3}{c|}{Inner Tracker} & \multicolumn{2}{c}{Calorimeters} \\
     & pixel & \sct & \trt & \lar & tile \\\hline
     channels \
     & $8\cdot10^7$ & $6.3\cdot10^6$ & $3.5\cdot10^5$ & $1.8\cdot10^5$ & \
     $9.8\cdot10^3$ \\
     dead & $0.050$ & $0.007$ & $0.025$ & $0.001$ & $0.017$ \\
   \end{tabular}
   \caption{Inner tracker and calorimeters}
  \end{subtable}
  \vskip 5mm
  \begin{subtable}{\textwidth}
    \centering
    \begin{tabular}{r | c c c c}
     & \multicolumn{4}{c}{Muon Spectrometer} \\
     & \msmdt & \msrpc & \mscsc & \mstgc \\\hline
     channels \
     & $3.5\cdot10^5$ & $3.7\cdot10^5$ & $3.1\cdot10^4$ \
     & $ 3.2\cdot10^5$ \\
     dead \
     & $0.003$ & $0.029$ \
	& $0.040$ & $0.018$  \
     \\
   \end{tabular}
   \caption{Muon spectrometer}
  \end{subtable}
  \caption{Number of channels and fraction of dead channels for each \atlas
   subdetector~\cite{twiki:dataqweird}.}
  \label{tab:dataqweird}
\end{table}
%
%

\section{Physics object reconstruction}
\label{sec:object}

From the raw data that passes the event filter (see Section~\ref{sec:trigger})
physics objects are reconstructed. As many particles can be detected by
several subdetectors, data from these subdetectors has to be combined into
a single reconstructed object. The methods and algorithms for reconstructing
these objects vary with the particle and the region of the detector where
the particle was detected. 

\subsection{Electrons}

Electron identification starts in the calorimeter~\cite{2012EPJC...72.1909A}.
In the central region of~$|\eta| < 2.47$, energy deposits in the calorimeter
are matched with a charged particle track from the inner detector. The
distance between the extrapolated track and the deposit must be smaller 
than~$0.05$ in~$\eta$ and, depending on the direction of curvature, smaller
than~$0.05$ or~$0.1$ in~$\phi$. If there are several tracks that match the
cluster, the one with the smallest distance is chosen.

The direction of a central electron is determined by the direction of the
matched track at the interaction point. The energy of the electron is
determined by the energy deposit in the cluster.

\smallskip
Since there are no tracking detectors in the forward region 
($2.5 < |\eta| < 4.9$), forward electrons are reconstructed using only the
energy deposits in the calorimeter. The direction of the electron is 
established by the center of the energy deposit, while the energy is
determined by the total energy deposit in the cluster, corrected for 
energy losses in the path before the calorimeter. 

\bigskip
Electron candidates are selected based on discriminating variables such as
the track quality, the number of hits in the \trt and the deposited energy
in the hadronic calorimeter. Three selections are made, each a subset of the
next: \emph{tight}, \emph{medium} and \emph{loose}\footnote{An 
    additional selection called \emph{multilepton} is made for 
        the 2012 dataset, optimized for specialized analyses. Detailed
    information on this selection can be found in~\cite{ATLAS-CONF-2014-032}.}.
    Each selection is optimized in bins of $|\eta|$ and \eT.
The efficiencies for identifying and reconstructing electrons with each
selection are shown in~Figure~\ref{fig:eleff}.  These figures show that the
reconstruction and identification efficiencies for loose electrons are very
close to~$100\%$, but that the efficiency for tight electrons is much lower.
The agreement between data and~\mc is reasonable.

For the analyses described in this thesis
the \emph{tight} selection is used. This selection has a 
lower efficiency than the other selections, 
      but has much lower fake rates~\cite{Aad:1694142,ATLAS-CONF-2014-032}.

\begin{figure}[pht]
\centering
\begin{subfigure}{0.45\textwidth}
  \centering
  \includegraphics[width=\textwidth]{figures/electron_id_pt7}
  \caption{Combined efficiency
  as a function of \eT{}
  for all selections in 2011.\\\phantom{x}}
  \label{eleff1}
\end{subfigure}
\quad
\begin{subfigure}{0.45\textwidth}
  \centering
  \includegraphics[width=\textwidth]{figures/electron_id_eta7}
  \caption{Combined efficiency for electrons with $25<\eT<30\unit{GeV}$
  as a function of $\eta$ for all selections in 2011.}
  \label{eleff2}
\end{subfigure}\\
\vskip -2mm
\begin{subfigure}{0.45\textwidth}
  \centering
  \includegraphics[width=\textwidth]{figures/electron_id_pt}
  \caption{Combined efficiency as a function of \eT{} for all selections
      in 2012, compared between monte carlo and data.
  }
  \label{eleff3}
\end{subfigure}
\quad
\begin{subfigure}{0.45\textwidth}
  \centering
  \includegraphics[width=\textwidth]{figures/electron_id_eta}
  \caption{Combined efficiency for electrons with $\eT>7\unit{GeV}$
    as a function of $\eta$ for all selections in 2012.}
  \label{eleff4}
\end{subfigure}\\
\vskip 2mm
\begin{subfigure}{0.45\textwidth}
  \centering
  \includegraphics[width=\textwidth]{figures/electron_id_pv}
  \caption{Combined efficieny for all selections as a function of the
      number of primary vertices in the event in 2011 and 2012 data 
  and monte carlo.}
  \label{eleff5}
\end{subfigure}
\caption{Electron reconstruction and identification efficiencies for 2011 and
2012 datasets.}
\label{fig:eleff}
\end{figure}

\subsection{Muons}

Muons are only reconstructed in the central region of $|\eta| < 2.7$, 
since the muon spectrometer does not extend beyond $|\eta| = 2.7$ (see
Section~\ref{sec:muonspec}) and muons can only be identified by the
inner tracker or the muon spectrometer. Depending on what information on the
muon is available, four types of muons can be reconstructed~\cite{Aad:1743068}:
\begin{itemize}
  \item \emph{Combined} (\muCB) muons, which are constructed by combining
          two independently reconstructed tracks from the inner tracker
          and the muon spectrometer.
  \item \emph{Stand-Alone} (\muSA) muons, which are constructed using only
  a track in the muon spectrometer. 
  \item \emph{Segment-tagged} (\muST) muons, which are constructed from
  a track in the inner tracker which matches a track segment in the muon
  spectrometer.
  \item \emph{Calorimeter-tagged} (\muCal) muons, which are constructed from
  a track in the inner tracker which matches an energy deposit 
  in the calorimeter which is compatible with a minimum-ionizing particle.
\end{itemize}
Two \emph{chains} that reconstruct muons exist in \atlas: 
Staco~\cite{Hassani:2007cy} and Muid~\cite{Lagouri:2003dt} (also called
chain 1 and chain 2 respectively), which have very
similar performance. For the 2012 data a third reconstruction chain has been
developed, incorporating the best features of both chains. 

The identification and reconstruction efficiencies are shown 
in Figure~\ref{fig:mueff}. One can clearly see the reduction in efficiency
for very low~$|\eta|$, where there is a gap in the coverage of the muon
spectrometer. The \muCal muons, which are reconstructed without the use
the the muon spectrometer, suffer much less from this gap.

\begin{figure}[pht]
\centering
\begin{subfigure}{0.47\textwidth}
  \centering
  \includegraphics[width=\textwidth]{figures/muon_id_7}
  \caption{Reconstruction efficiency of Staco (chain 1) 
  during the 2011 datataking period.}
  \label{fig:mueffa}
\end{subfigure}
\quad
\begin{subfigure}{0.47\textwidth}
  \centering
  \includegraphics[width=\textwidth]{figures/muon_id_8a}
  \caption{Reconstruction efficiency of Staco (chain 1) 
  during the 2012 datataking period.}
  \label{fig:mueffb}
\end{subfigure}\\
\vskip 0.5cm
\begin{subfigure}{0.47\textwidth}
  \centering
  \includegraphics[width=\textwidth]{figures/muon_id_8b}
  \caption{Reconstruction efficiency of Muid (chain 2) 
  during the 2012 datataking period.}
  \label{fig:mueffc}
\end{subfigure}
\quad
\begin{subfigure}{0.47\textwidth}
  \centering
  \includegraphics[width=\textwidth]{figures/muon_id_8c}
  \caption{Reconstruction efficiency of the third chain 
  during the 2012 datataking period.}
  \label{fig:mueffd}
\end{subfigure}
\caption{Reconstruction efficiency of muons as a function of rapidity.
Figure~\subref{fig:mueffa} shows the efficiency during the 2011 run, while
figures~\subref{fig:mueffb} through~\subref{fig:mueffd} show the efficiencies
of all three chains during the 2012 run. The low efficiencies around 
$|\eta| = 1$ during the 2011 run are caused by missing muon chambers, which
were installed after the 2011 run.}
\label{fig:mueff}
\end{figure}



\subsection{Jets}

Jets in \atlas are reconstructed using only data from the calorimeter.
The clusters in the calorimeter are resolved using the anti-\kt 
algorithm~\cite{2008JHEP...04..063C}, with a radius parameter of $0.4$.
This algorithm works well in the case of 
large numbers of soft jets. 

The largest uncertainty in the reconstruction of jets is on the 
\emph{jet energy scale} (\jes), which is of the order of $5\%$, depending
on the \pt of the jet. Figure~\ref{fig:JES} shows the \jes uncertainty
on 2011 data.

Several b-tagging algorithms, which try to distinguish light jets from
b-jets, are deployed in \atlas. The often used \mvone tagger
is a neural network that combined the output of three other b-tagging
algorithms~\cite{ATLAS-CONF-2012-097}. Each b-tagging algorithm can be
used at a multitude of working points, which define the efficiency and
the mistagging rates of the tagger. A performance graph of the \mvone tagger
is shown in Figure~\ref{fig:btag}.

\begin{figure}[pth]
  \centering

\begin{subfigure}{0.47\textwidth}
  \centering
  \includegraphics[height=0.6\textwidth]{figures/JES}
  \caption{Jet energy scale uncertainty as a function of 
      $p_\mathrm{T}^\mathrm{jet}$ for anti-\kt jets in 2011 
          data~\cite{Aad:2014bia}.\\\phantom{x}
  }
  \label{fig:JES}
\end{subfigure}
\quad
\begin{subfigure}{0.47\textwidth}
  \centering
  \includegraphics[height=0.6\textwidth]{figures/btag}
  \caption{Performance of the \mvone b-tagging algorithm on 
      $\sqrt{s} = 8\unit{TeV}$ simulation of $t\bar{t}$ 
          events~\cite{ATLAS-CONF-2014-046}.}
  \label{fig:btag}
\end{subfigure}
  \caption{Jet energy scale uncertainty and b-tagging performance plots.}
  \label{fig:jetperf}
\end{figure}

\subsection{Missing transverse energy}

The missing transverse energy (\met) has two 
contributions~\cite{2012EPJC...72.1844A}.
A calorimeter missing transverse energy is calculated
using all energy deposits in the calorimeter. Low-\pt tracks from the 
inner detector are added if the particles are missing in the calorimeters.

The second contribution is the  muon missing transverse 
energy, which is established using all muons detected in the
muon spectrometer, and using tracks in the inner detector in regions where the
muon spectrometer has no coverage.


The resolution of the \met, split up in components of 
$E_{\mathrm{x}}^{\mathrm{miss}}$ and
$E_{\mathrm{y}}^{\mathrm{miss}}$, where 
\begin{equation}
\met = \sqrt{(E_{\mathrm{x}}^{\mathrm{miss}})^2
+            (E_{\mathrm{y}}^{\mathrm{miss}})^2},
\end{equation}
as a function of the total transverse energy of an event 
is shown in Figure~\ref{fig:etmiss78}. For~$\sqrt{s} = 8\unit{TeV}$, 
several pile-up suppression techniques are applied to prevent the deterioration
seen in \met resolution due to increasing pile-up~\cite{ATLAS-CONF-2013-082}.


\begin{figure}[pht]
\centering
\begin{subfigure}{0.47\textwidth}
  \centering
  \includegraphics[width=\textwidth]{figures/etmiss7}
  \caption{Missing energy resolutions in monte carlo 
      for $\sqrt{s} = 7\unit{TeV}$ events.}
  \label{fig:etmiss7}
\end{subfigure}
\quad
\begin{subfigure}{0.47\textwidth}
  \centering
  \includegraphics[width=\textwidth]{figures/etmiss8}
  \caption{Missing energy resolutions in data and monte carlo 
      for $\sqrt{s} = 8\unit{TeV}$ events.}
  \label{fig:etmiss8}
\end{subfigure}
\caption{Resolution of $E_{\mathrm{x}}^{\mathrm{miss}}$
  and $E_{\mathrm{y}}^{\mathrm{miss}}$ as a function of the total transverse
  energy of the event~\cite{ATLAS-CONF-2010-057,Aad:2012re,ATLAS-CONF-2013-082}.
  }
\label{fig:etmiss78}
\end{figure}
\section{Simulation}

In order to study the detector response for various physics processes,
simulated \emph{monte carlo} (\mc) data samples are produced.
The simulation process in \atlas consists of three 
stages~\cite{2010EPJC...70..823A},
each of which will be briefly discussed in the following subsections.

\subsection{Generation}
An event generator uses a matrix element (or a set of matrix elements) to
produce complete events starting from a proton-proton
(or nucleon-nucleon) initial state. Event generators also handle immediate
decays, such as decays of top quarks, and hadronization.
The output is a tree of all particles and their decay products.

Many event generators are used in \atlas, including
\pythia{}~\cite{Sjostrand:2006za}, \herwig{}~\cite{Corcella:2000bw}, 
Sherpa~\cite{Gleisberg:2008ta}, 
Powheg~\cite{Nason:2004rx}, 
Alpgen~\cite{Mangano:2002ea}, \mcatnlo{}~\cite{Frixione:2002ik} 
MadGraph~\cite{Alwall:2011uj} and \acermc{}~\cite{Kersevan:2004yg}. 
The \pythia and \herwig generators are the benchmark generators of
\atlas, to which all other generators are compared. 
While both \pythia and \herwig have new versions written in \cpp,
older well-tested versions (\pythia 6.4 and \herwig 6.5) written in 
the very old
\fortran language are used.


Powheg, Alpgen, \acermc, MadGraph and \mcatnlo generate 
only the matrix element interaction,
and write out the result in the \emph{Les Houches} format~\cite{Boos:2001cv},
leaving the decay and hadronization to for example 
\pythia or \herwig. Each of these generators targets a specific type of 
processes: Alpgen specializes in final states with several well-separated
jets, \acermc is designed for producing events with $W$ or $Z$ bosons with
several jets, and Powheg and \mcatnlo generate
hard scattering events at next to leading order.

Sherpa is expected to perform better for events with large numbers of
isolated jets in the final state and 
interfaces with \pythia's hadronization model, making it a complete generator.

While many of these generators generate the matrix element at leading order,
some generators include one-loop corrections. Powheg and \mcatnlo are
examples of such next-to-leading order generators. 

\smallskip
In the generation process \emph{parton density functions} (\pdf{}s) are used to 
approximate the structure of the protons. \Atlas uses
the {\sc lhapdf}~\cite{Bourilkov:2006cj} library of \pdf{}s, from which
the {\sc qtec}~\cite{{Salihagic:938644}} \pdf{}s are default. An alternative
set op \pdf{}s is the \herapdf library~\cite{sjakies}.

\subsection{Detector simulation}
\label{sec:sim}
The output of the event generator is fed into the detector simulation,
which simulates the interactions of the particles with the detector,
tracking their propagation through the detector. The simulation is done
by \geant~\cite{Agostinelli:2002hh}, for which a very detailed model
describing the detector has been made.
This model contains over $300\,000$ individual volumes, more than
half of which belong to the calorimeters.

Energy deposits in the sensitive regions of the detectors,
called \emph{hits}, are stored as output, together with the time and place
coordinates of the deposit. 

\smallskip
The vertex position of the generated collision is not always placed in the
exact center of the detector: the vertices are smeared according to the 
ideal luminous region of the \lhc in \atlas, and can be rotated in $\phi$
in order to produce a more accurate approximation of reality.

\subsection{Digitization}

The response of the detector is simulated in the digitization step. 
The voltage on each readout channel is calculated, taking into account
noise, cross-talk, and channel-dependent variation such as dead channels.
The simulated detector output is then fed into the triggering and 
reconstruction algorithms that are also used for data. The result is a file
with simulated events in a format similar to the data files.

\subparagraph{Pile-up}
In order to simulate pile-up, hits of simulated signal events are 
combined with background
events, such as minimum bias, cavern background, and beam halo. The
number of events per simulated bunch crossing is varied according to the 
distribution of pile-up in data.

\subsection{Truth}
The generated events, before hadronization and shower, 
    are stored together with the
results of the entire simulation process, and are called \emph{(monte carlo)
    truth}. This truth information is useful for studies targeting 
the detector response, for example in \emph{folding} or \emph{unfolding} (see
Section~\ref{sec:likfit}).

\subsection{Fast simulations}

The detector simulation method described above is very compute-intensive, taking
on average over half an hour per $\ttbar$ event~\cite{1742-6596-331-3-032053}.
By far the largest fraction ($\approx95\%$) of time is spent in the simulation
of the calorimeter.

\subparagraph{\atlfastone}
This first fast simulation does not provide any realistic 
detector description, making detector performance studies impossible.
Any simulated track is passed on as reconstructed object, bypassing
object reconstruction. Therefore misreconstruction effiencies are not modeled,
       and only fake b-jets and taus are modeled by applying jet flavor
tagging effiencies.

\subparagraph{\atlfasttwo} 
In contrast to \atlfastone, the second fast simulation simulates all properties
of a track using parametrizations obtained with the full simulation, and
produces hits which are passed on to the reconstruction algoritms. This has
the advantage that the final simulation output has the same format as that
of the full simulation, and monte carlo sets generated by the two methods can
therefore be used together in one analysis.

\subparagraph{Fast {\sc g4} simulation}
A \geant simulation which only treats electromagnetic showers in the sensitive
regions of the calorimeter reduces the required computing time by a factor
of three, without sacrificing effiency. As the output format is exactly the same
as that of the full simulation, it too can be used together with full simulation
and \atlfasttwo monte carlo sets.

%*****************************************
%*****************************************
%*****************************************
%*****************************************
%*****************************************
