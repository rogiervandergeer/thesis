%************************************************
\chapter{The LHC and ATLAS}\label{ch:detector}
%************************************************

\section{The Large Hadron Collider}\label{sec:LHC}
The Large Hadron Collider (\lhc) is a proton-proton collider
situated at the European Organization for Nuclear Research (\cern) in
Geneva, Switzerland~\cite{LHC:JOI}. 
It is a synchrotron with a circumference of $27\unit{km}$, 
and is built in the tunnel of the 
former~Large~Electron-Positron~Collider~(\lep).
It was designed to collide proton beams with a center-of-mass energy 
of~$14\unit{TeV}$ at a luminosity of $10^{34}\unit{cm^{-2}s^{-1}}$.

The two beams of the \lhc cross at four points in the tunnel. At
these interaction points four large experiments have been built:
\begin{itemize}
    \item \textbf{A Toroidal} \lhc \textbf{ApparatuS} (\atlas) is a 
    general-purpose detector designed to explore the full spectrum of
    proton-proton physics. A detailed description of \atlas follows
    in section~\ref{sec:ATLAS}.

    \item \textbf{Compact Muon Solenoid} (\cms) is also a general-purpose 
    detector, with a similar physics programma as \atlas~\cite{CMS:JOI}.

    \item \textbf{Large Hadron Collider beauty} (\lhcb) is a forward detector
    designed especially for studying rare B-meson decays~\cite{LHCb:JOI}.

    \item \textbf{A Large Ion Collider Experiment} (\alice) is an experiment
    designed for studying lead-lead collisions, which the \lhc can also
    produce~\cite{ALICE:JOI}.
\end{itemize}

The startup of the \lhc took place in summer 2008, but was quickly followed
by a magnet quench accident. After a long period of repairs it was restarted
at the end of 2009, and in the years 2010, 2011 and 2012 three datasets
(\emph{runs}) were taken. As the luminosity increased greatly during the 2010
run, the two latter datasets are much larger. During 2010 and 2011 the 
center-of-mass energy of the \lhc was $\sqrt{s} = 7\unit{TeV}$, while
in 2012 it was $\sqrt{s} = 8\unit{TeV}$. The total delivered luminosity
was $5.46\unit{fb}^{-1}$ and $22.8\unit{fb}^{-1}$ in 2011 and 2012 respectively.
See Section~\ref{sec:runcon} for more details.

\section{The ATLAS experiment}\label{sec:ATLAS}
The \atlas detector was designed to
cover the full frontier of proton-proton physics~\cite{ATLAS:JOI}. 
In the design the following physics goals were set:
\begin{itemize}
  \item large acceptance of almost the full solid angle,
  \item good charged-particle momentum resolution and reconstruction efficiency,
  \item very good electromagnetic calorimetry and full-coverage hadronic
  calorimetry,
  \item good muon identification and momentum resolution,
  \item highly efficient triggering.
\end{itemize}

The Standard Model Higgs-boson, which has been discovered by \atlas in
2012~\cite{Aad:2012tfa}, was an important benchmark during the design
process of the \atlas detector, together with new heavy gauge bosons
and supersymmetry.

\subsection{Coordinate system}\label{sec:coordinates}
The origin of the right-handed \atlas coordinate system lies in 
the nominal interaction
point. The positive~$x$-axis points to the center of the \lhc ring, the
positive~$y$-axis points up (\ie away from the center of the earth) so that
the~$z$-axis is parallel to the beam axis. The $x$-$y$-plane is called
the \emph{transverse} plane, being perpendicular to the beam axis.
The azimuthal angle (around the beam axis)
is denoted by~$\phi$ and the polar angle (from the beam axis) is 
denoted\footnote{The naming convention in physics is opposite to the one
    in mathematics, where the azimuthal angle is denoted by~$\phi$
        and the polar angle is denoted by~$\theta$.
       This may be confusing for mathematicians.} by~$\theta$.

From the polar angle $\theta$ a \emph{pseudorapidity} is defined 
as~$\eta \equiv -\ln \tan (\theta / 2)$.
Transverse properties, like the transverse momentum \pt\ and the missing
transverse energy~\met, are projected onto the transverse plane.

\subsection{Detector layout}
\begin{figure}[p]
\includegraphics[angle=90,width=\textwidth]{figures/0803012_01.jpg}
\caption{A computer-generated overview of the \atlas detector.}
\label{fig:ATLAS}
\end{figure}
Three detector subsystems of \atlas are positioned enclosing eachother 
around the interaction point. Magnet subsystems provide all 
the detector subsystems with a 
magnetic field. 

Each subsystem is divided in a \emph{barrel} and 
two~\emph{endcaps}. The barrel has the form of a cylinder with the beam
line in its axis, serving the \emph{central} (low absolute pseudorapidity)
 region. The endcaps
close off the cylinder of the barrel, serving the \emph{forward}
(high absolute pseudorapidity) regions.

The magnet system and each of the detector subsystems will
be briefly discussed in the following sections.



\subsection{Magnets}
In the center of the detector a superconducting solenoid with an axial length
of $5.8\unit{m}$ and a diameter of $2.5\unit{m}$ encloses the inner tracker
and provides it with a $2\unit{T}$ axial field. 

The much larger superconducting toroidal magnets from 
which \atlas derives its name can be found inside the muon spectrometer
and consists of one barrel part and two endcaps. Each of these magnets contains
eight superconducting coils, providing a field of $0.5$ to $1\unit{T}$ average
in the
muon spectrometer. The magnetic field integrated over the path of a muon ranges
from $3\unit{Tm}$ in the barrel to $6\unit{Tm}$ in the endcaps.

\subsection{Inner tracker}
\begin{figure}[tb]
\includegraphics[width=\textwidth]{figures/0803014_01.jpg}
\caption{A computer-generated overview of the \atlas inner tracker.}
\label{fig:innertracker}
\end{figure}

The \atlas inner tracker is composed of three subsystems: which are,
from the inside out: the pixel detector,
the silicon strip detector (\sct) and 
the Transition Radiation Tracker (\trt)~\cite{atlas:tracker:TDR}.
The layout of the inner tracker is shown in Figure~\ref{fig:innertracker}.
Both the pixel detector and \sct are silicon detectors, while the
\trt is a gas filled detector. The inner tracker is fully enclosed in the
solenoid magnet.

The pixel detector is designed to be a very accurate tracker as close to
the beam line as possible. In the barrel it consists of three concentric
cylinders with radii of about~$5.9\unit{cm}$,~$12\unit{cm}$ and~$14\unit{cm}$,
while the endcaps consist of four disks. The pixel chips are made 
of~$250\unit{\upmu m}$ thick n-type silicon, equivalent to less 
than~$1.39\%$ of a radiation length. All layers combined contain
over 80 million pixels, which amounts to the vast majority of all
\atlas readout channels. 

\begin{table}[hb]
\begin{center}
\begin{tabular}{l | c c | c c|}
& \multicolumn{2}{c|}{Barrel} & \multicolumn{2}{c}{End caps} \\
& $R\phi$ & $z$ & $R\phi$ & $R$ \\\hline
pixel & $10\unit{\upmu m}$ & $115\unit{\upmu m}$ & $12\unit{\upmu m}$ & $77\unit{\upmu m}$ \\
\sct & $17\unit{\upmu m}$ & $580\unit{\upmu m}$ & $17\unit{\upmu m}$ & $580\unit{\upmu m}$ \\
\end{tabular}
\caption{Intrinsic spatial detector resolution.}
\label{tracking:resolution}
\end{center}
\end{table}

\smallskip
The \sct consists of silicon microstrip detectors. The strips
measure~$6.4\unit{cm}$ by~$80\unit{\upmu m}$, and thus alone provide accuracy
in one direction ($\phi$) only. 
By using two modules of strip detectors under a slight
angle with respect to eachother, 
      some accuracy in the $z$-direction ($\theta$ for the endcap) 
    is restored.
The \sct has four layers of detectors in the barrel region, and 
nine disks in each endcap.

The \sct sensors are made of a much cheaper classic p-in-n silicon with a 
thickness of about~$285\unit{\upmu m}$.
Due to the one-dimensional granularity the \sct and despite its larger size,
the~6.3~million readout channels of the \sct are a fraction of that of the 
pixel detector.

The tracking resolution of the pixel detector and the \sct can be found in 
Table~\ref{tracking:resolution}.

\smallskip
The \trt is a straw tube detector with electron identification capabilities.
It contains total of~$3.7\cdot10^{5}$ straw tubes with a diameter 
of~$4\unit{mm}$ and a length
varying between~$150\unit{cm}$ in the barrel down to~$20\unit{cm}$ in some end cap regions.
In the barrel the tubes are oriented axially, while in the end caps they
are oriented radially. The barrel tubes are electrically separated
at the center to reduce occupancy, resulting in double the number of
readout channels in the barrel. The total number of readout channels of
the \trt is~$4.2\cdot10^{5}$.

An average track traversing the \trt produces 36 hits, making pattern
recognition relatively easy, and resulting in a tracking accuracy of 
$130\unit{\upmu m}$ (in one dimension). Electron identification is done
by using xenon gas to detect transition radiation photons produced between
the straws.

\bigskip
To achieve tracking resolutions as shown in Table~\ref{tracking:resolution}
the detector has to be properly aligned: \ie deformations in the detector
structure have to be measured accurately in order to reconstruct tracks
correctly. While the inner tracker is installed in the \atlas detector its
alignment can be measured in two ways: by using information from 
measured tracks
and by using optical alignment measurements.
Accurate alignment using tracks can only be done if the detector is
very stable in time, or if the alternative alignment methods can be used to
correct for short term distortions. 

In the pixel detector and the \sct two optical alignment methods are used:
\emph{straightness monitors} which measure the deviation from a straight line
with mutiple translucent photosensors in a laser beam, and {frequency
scan interferometry}~\cite{Coe:2002td} to measure distances. 
Using these methods, pixel sensors
can be located with an precision down to about $10\unit{\upmu m}$.
In the \trt the alignment requirements are such that track alignment 
alone is sufficient.

\bigskip
Together the three subsystems of the inner tracker achieve 
a momentum resolution of $\sigma \approx 1.5\%$ for low-\pT{} tracks
and of $\sigma\left(\pT^{-1}\right) \approx 0.4\unit{TeV}^{-1}$ for high-\pT{}
central tracks. The impact parameter resolutions are as small 
as $\sigma(d_0)\approx13\unit{\upmu m}$ and $\sigma(z_0)\approx75\unit{\upmu m}$
for central $25\unit{GeV}/c$ tracks, and the inner tracker has sensitivity over 
the full $|\eta| \leq 2.5$ range. 

\subsection{Calorimeter}
\begin{figure}[tb]
\includegraphics[width=\textwidth]{figures/0803015_01.jpg}
\caption{A computer-generated overview of the \atlas calorimeter.}
\label{fig:calorimeter}
\end{figure}

The calorimeter is located outside of the solenoid magnet. It is divided into
a liquid argon filled calorimeter up to a detector radius of $2.2\unit{m}$
and an iron-scintillator tile 
calorimeter with an outer radius 
of~$4.2\unit{m}$ 
which surrounds the liquid argon detector~\cite{atlas:liq:TDR,atlas:tile:TDR}. 
A schematic of the calorimeter layout is shown in Figure~\ref{fig:calorimeter}.

The electromagnetic (\emc) 
calorimeter is designed to have a much smaller granularity
than the hadronic calorimeter, while the hadronic calorimeter contains much
more material in order to stop the high energetic
 (hadronic) particles and to minimize
escaping of such particles into the muon spectrometer. The granularity of each
calorimeter subdetector is summarized in Table~\ref{tab:calgran}.

\bigskip
The liquid argon calorimeter is contained in three cryostats: one barrel
and two endcaps. The barrel cryostat is shared with the solenoid magnet.
The endcap cryostats contain an electromagnetic calorimeter as well as
an hadronic liquid argon calorimeter, while the
tile calorimeter acts as hadronic calorimeter in the barrel region. 
A forward calorimeter (\fcal) with an outer radius of $47\unit{cm}$
is also situated inside the calorimeter endcaps. 

\smallskip
The barrel liquid argon calorimeter, which covers the range 
$0 < |\eta| < 1.475$, is divided into two half-barrels, 
which each consist of 1024 lead-stainless-steel converters covered with 
electrodes and with readout
boards in between. The converters are shaped like an accordion to
create a large surface while maintaining
continuity in $\phi$. As a function of pseudorapidity the amount of material
in the barrel liquid argon calorimeter varies from about 1 to 2 absorption
lengths, and 20 to 30 radiation lengths.

The endcap electromagnetic calorimeters ($1.375 < |\eta| < 3.2$)
 each consist of two concentric wheels of 
accordion-shaped converters, and otherwise have a design similar to that
of the barrel part.

\smallskip
The hadronic endcap calorimeters do not use the accordion electrode structure,
but use flat copper plates. Each endcap consists of two wheels, one with
$25\unit{mm}$ thick plates, and a second with $50\unit{mm}$ thick plates.
The thickness of these plates is chosen such that the amount of material
in the hadronic endcap calorimeter in terms of absorption lengths is about
10, similar to that of the tile calorimeter.

The \fcal ($3.1<|\eta|<4.9$) 
consists of a grounded metal matrix with channels filled with
concentric high-voltage rods and tubes. The liquid argon gap between the rod
and the tube can be as narrow as $250\unit{\upmu m}$. With a density of up to
$14.5\unit{g/cm^3}$ the \fcal is much denser than the other calorimeters,
leading to an average of about 10 absorption lengths of material, similar
to that of the hadronic calorimeters.

\bigskip
The tile calorimeter is split up into one barrel and two \emph{extended barrel}
parts, which have the form of a barrel around the liquid argon endcaps. 
A gap of $60\unit{cm}$ exists between the barrel and extended barrel parts.
All parts contain steel tiles with scintillators in between. Light produced
by the scintillators is transported out of the calorimeter using optical fibers.

Just like the endcap hadronic calorimeter and the \fcal, the barrel tile 
calorimeter amounts to about 10 absorption lenghts of material, while the
material in the extended barrel varies from about 7 to 13 absorption lengths as
a function of pseudorapidity.

\begin{table}[ht]
\begin{center}
\begin{tabular}{r | r @{$\times$}l }
& $\Delta\eta$ & $\Delta\phi$ \\\hline
\emc barrel & 0.025 & 0.025 \\
\emc endcap & 0.025 & 0.1   \\
\fcal & 3.0 & 2.6 \\
Had.\ endcap & 0.1 & 0.1 \\
Tile barrel & 0.1 & 0.1\\
Tile endcap & 0.1 & 0.1\\
\end{tabular}
\caption{Granularity per calorimeter subdetector. In some subdetectors
the granularity varies by pseudorapidity region or by layer.
 In such cases the best granularity is quoted.}
\label{tab:calgran}
\end{center}
\end{table}

\bigskip
The calorimeters have an energy resolution below~$5\%$ for electrons,
and an angular resolution of~$< 1.5^\circ$ for particles up 
to~$300\unit{GeV}$.
The coverage of the calorimeters goes up to~$|\eta| < 4.7$, leaving 
only a very
small part of the solid angle uncovered.

\subsection{Muon spectrometer}
\label{sec:muonspec}
\begin{figure}[tb]
\includegraphics[width=\textwidth]{figures/0803017_01.jpg}
\caption{A computer-generated overview of the \atlas muon spectrometer.}
\label{fig:muonspectrometer}
\end{figure}

The \atlas muon spectrometer consists of a precision system and a trigger
system, arranged
such that each muon traverses at least three layers of precision chambers
and at least one layer of trigger chambers~\cite{atlas:muon:TDR}.
 The barrel of the muon spectrometer
is interspersed between the barrel toroid magnet coils, 
while in the endcaps this is not possible.

\bigskip
Precision track measurements are done by two types of chambers:
\emph{monitored drift tube} chambers (\msmdt) and \emph{cathode strip chambers}
(\mscsc). The \mscsc{}s are only used in the very forward regions as they
have a finer granularity so that they can cope with the higher flux of
muon and large backgrounds.

\smallskip
The \msmdt chambers are gas-filled detectors using $30\unit{mm}$ diameter
aluminum tubes with an anode wire along the axis of the tube. The resolution
on the drift radius
of a single wire is $80\unit{\upmu m}$, and each chamber incorporates several
layers (6 to 8) of tubes to achieve an accuracy below $50\unit{\upmu m}$
in the direction of the sagitta. 
The length of the tubes varies from $70\unit{cm}$ to over $6\unit{m}$.

An \msmdt chamber consists of an aluminum frame with several layers of tubes
on each side. The frame is designed such that the gravitational bending of the  
tubes is similar to the gravitational sag of the wires. The frame also contains
an alignment system which is described in Section~\ref{sec:muon:align}.

The almost 1200 \msmdt chambers together cover a total of $5500\unit{m^2}$ and
have $3.7\cdot{10^5}$ readout channels.

\smallskip
The \mscsc{}s are multiwire proportional chambers with an anode wire pitch 
and anode-cathode spacing of $2.54\unit{mm}$. The cathode strips, which
are oriented perpendicular to the anode wires, have a spacing 
of~$5.08\unit{mm}$. Resolutions better than $60\unit{\upmu m}$ can be achieved
by charge interpolation using multiple strips. 
The \mscsc{}s also have a good time resolution of $7\unit{ns}$.

Only 32 \mscsc{s} are installed in \atlas, covering $27\unit{m^2}$. Due to the
high density of wires these chambers combined have $67\cdot10^3$ channels.

\bigskip
The trigger chambers of the muon spectrometer provide the trigger system
with a muon $\pt$-cutoff for low-\pt{} muons. They also provide bunch crossing
identification, as the \msmdt precision chambers do not have the time resolution
to do so, and a coarse ($5$ to $10\unit{mm}$) measurement of the second 
coordinate of a track. Two types of trigger chambers are used in the \atlas
muon spectrometer.

\smallskip
\emph{Resistive plate chambers} (\msrpc) are the trigger chambers used in the
barrel region. They consist of a narrow gas-filled gap between two resistive
plates, with a $4.5\unit{kV}$ field acrross the gap.
The \msrpc{}s have a time resolution of $1.5\unit{ns}$, which is more than
sufficient to identify bunch crossings at intervals of $25\unit{ns}$.
The \msrpc{s} have a total of $35\cdot{10^4}$ channels,
    and a time resolution of $1.5\unit{ns}$.


\smallskip
Triggering in the endcaps is done by \emph{thin gap chambers} (\mstgc).
These \mstgc{}s are similar to the \mscsc{}s, but have an anode pitch
which is larger than the anode-cathode spacing and use a quenching gas mixture.
Up to 7 layers of \mstgc{}s are used per \msmdt chamber, which leads to
a total of $44\cdot{10^4}$ channels,
  and a time resolution of $4\unit{ns}$.

\bigskip
The muon spectrometer covers the full rapidity region of $|\eta|< 2.7$, except
for a gap at $\eta = 0$. Muon momentum resolution is
$\sigma\approx2\%$ for muons with~$\pT = 10\unit{GeV}/c$, and goes up to
$\sigma\approx11\%$ for muons with~$\pT = 1\unit{TeV}/c$, while
the angular resolution
of~$\phi$ is on average~$1\unit{mrad}$. The trigger efficiency of the
muon spectrometer trigger system is~$>90\%$ for~$\pT = 25\unit{GeV}/c$ central
muons.

\section{Muon spectrometer alignment}\label{sec:muon:align}

The \atlas muon spectrometer is designed 
to provide a \pt-resolution of~$10\%$ for~$1\unit{TeV}/c$ 
muons~\cite{muonalignment}.
The minimum sagitta (see Figure~\ref{fig:sagitta}) of a $1\unit{TeV}$ muon track
(at~$\eta=0$) is $500\unit{\upmu m}$, meaning that the error on the measurement
of the sagitta must be less than $50\unit{\upmu m}$. 
To reach this level of precision, the alignment of the muon spectrometer
precision chambers must be very well known. 
Like in the inner tracker, the alignment of the muon spectrometer can be 
determined by using information from fitted tracks and by using an optical
alignment system. 

\begin{figure}[t]
  \centering
  \includegraphics[width=0.9\textwidth]{figures/sagitta}
  \caption{Definition of the sagitta: in a three-point track measurement,
      the sagitta is the distance from the middle point of the three to
        the straight line connecting the two exterior points.}
    \label{fig:sagitta}
\end{figure}

\bigskip
The muon spectrometer barrel and endcaps each have their own optical alignment
system. In each of these subdetector two types of sensors are used. The \rasnik
system is used in all parts of the
muon spectrometer, while the \mssacam system is used only in the barrel and the
\msbcam system is used in both 
endcaps~\cite{Barriere:1081769,1748-0221-3-11-P11005}.

\subsection{The \rasnik system}

The Red Alignment system \nikhef (\rasnik) is the main alignment system
for the \atlas muon spectrometer~\cite{vanderGraaf:1073160}. It is a
three-point straightness monitor, consisting of three components:
a checkerboard mask,
  backlit by an infrared led through a 
diffusor, is projected
onto a {\sc ccd} sensor through a lens (see {Figure}~\ref{fig:rasnik}). An 
infrared-passthrough filter is mounted in front of the {\sc ccd} sensor 
to block any stray (visible) light. 

\begin{figure}[tb]
\includegraphics[width=\textwidth]{figures/rasnik.pdf}
\caption{A schematic view of a \rasnik system. Infrared light from an
illuminated coded mask
is projected onto a sensor via a lens. }
\label{fig:rasnik}
\end{figure}

\begin{figure}[tb]
\begin{center}
\includegraphics[width=0.5\textwidth]{figures/rasnik-pattern.eps}
\end{center}
\caption{A \rasnik mask image projected onto a sensor. The code
lines that replace the checkerboard pattern every ten lines are
clearly visible. }
\label{fig:mask}
\end{figure}

By analyzing the size, position and angle of the checkerboard pattern projected
onto the sensor, the relative position of the three components can be 
reconstructed. As only a small part of the mask is projected onto the sensor,
the mask is coded such that software can identify the projected section.
An example of the image created by a mask projected onto a sensor is shown in
Figure~\ref{fig:mask}.

Depending on the requirements of the system, the distance between the
sensor and the mask can range from a few $\unit{cm}$ up to over $10\unit{m}$.
A \rasnik system with the lens halfway between the sensor and the mask
has a resolution for off-axis
translations of $1\unit{\upmu m}$. The magnification 
of the projected image, which is used to measure the position in the 
longitudinal direction, can be determined with a resolution of~$2\cdot10^{-5}$.

\subsection{The \mssacam system}

The Saclay Camera system (\mssacam) is much alike the \rasnik system, but 
instead of a checkerboard mask a plate with four laser diodes is used 
(see Figure~\ref{fig:sacam}). The
camera and the lens are always mounted in the same housing, making this
a two-point alignment system. Due to the large ratio of light source to lens
and lens to sensor distances, this sensor is not very sensitive to translations,
but much more so for rotations. The resolution for rotations of this
system is $0.5\unit{mrad}$,  and the resolution for magnification
 is~$5\cdot10^{-4}$.

\begin{figure}[p]
\centering
\begin{subfigure}{0.45\textwidth}
  \centering
  \includegraphics[scale=0.7]{figures/sacam.pdf}
  \caption{\mssacam: Light from four (two shown)
  laser diodes is projected onto a sensor.}
  \label{fig:sacam}
\end{subfigure}
\quad
\begin{subfigure}{0.45\textwidth}
  \centering
  \includegraphics[scale=0.7]{figures/bcam.pdf}
  \caption{\msBcam: two sensors face eachother. Multiple systems can
  be linked.}
  \label{fig:bcam}
\end{subfigure}\\
\caption{Schematic views of the \mssacam and \msbcam alignment sensors.}
\label{fig:bcamsacam}
\end{figure}


\subsection{The \msbcam system}

The housing of the Brandeis {\sc ccd} Angular Monitor (\msbcam) 
contains all three components: the sensor, the lens, and two laser diodes.
Two \msbcam housings facing eachother therefore compose two complete alignment
systems facing in opposite directions, as shown in Figure~\ref{fig:bcam}.
Two \msbcam{}s mounted in the same housing are used to create concatenated
alignment lines. The intrinsic 
rotational resolution of this system is~$50\unit{\upmu m}$. In practice
this is limited by the precision of the \msbcam mount. As the distances between
the muon chambers in the endcaps are much larger than in the barrel, this
system was designed to cover much larger distances than the \mssacam.

\subsection{Barrel alignment}

\begin{figure}[p]
\includegraphics[width=\textwidth]{figures/rasnik-barrel.eps}
\caption{A computer generated view of a part of the \atlas muon spectrometer
barrel. The alignment systems are indicated with red lines.}
\label{fig:barrelalign}
\end{figure}

\begin{figure}[tb]
\includegraphics[width=\textwidth]{figures/inplane.pdf}
\caption{A schematic view of the inplane \rasnik alignment systems in 
barrel \msmdt chambers. Four optical lines are constructed using two
sensors.}
\label{fig:inplane}
\end{figure}
The barrel \msmdt chambers are arranged into 16 \emph{sectors} in $\phi$,
 which follow the eightfold symmetry of the toroid magnet. Each of these
sectors consists of three layers of 8 to 12 chambers. An overview of the
chambers and the alignment systems three half-sectors in shown in
Figure~\ref{fig:barrelalign}.




Chamber deformations are measured by \emph{inplane} \rasnik systems. Four
\rasnik systems are installed in every chamber (see Figure~\ref{fig:inplane}).
Two sensors are mounted on a support bar on one side of the chamber, 
four lenses in holes in a support bar in the middle, and four masks on the
support bar at the end opposite of the sensors. 
The sensors are shared by two lines. 
Switching the {\sc led}s to illuminate only one of the two masks
allows subsequent individual measurements for each line.
Two holes are made in longitudinal
support structures to allow the light of the diagonal lines to reach
the sensors. 


Some chambers\footnote{These are the \emph{barrel inner small} ({\sc bis}) 
chambers.}, which are placed 
between the toroid magnet coils and the calorimeter and are therefore rather
small, have only one inplane \rasnik system, as shown 
in Figure~\ref{fig:smallinplane}.

\smallskip
The \emph{axial} and \emph{praxial} systems measure the relative positions
of all chambers in a sector. The
axial \rasnik systems run along the short edges of multiple chambers,
and can be seen  in Figure~\ref{fig:barrelalign}. 
At all corners each chamber is connected
to the corners of adjacent chambers with two crossing praxial \rasnik systems.
These systems are very short (of the order of $10\unit{cm}$), and have
the sensor and lens built into one package, similar to the \msbcam and \mssacam
systems.


\begin{wrapfigure}{o}{0.5\textwidth}
  \vspace{-20pt}
  \begin{center} 
   \includegraphics[width=0.48\textwidth]{figures/inplanesmall.pdf}
  \end{center}
  \vspace{-20pt}
  \caption{Layout of the inplane alignment \rasnik system on 
\emph{barrel inner small} \msmdt chambers. The color coding is the same
as in Figure~\ref{fig:inplane}.}
  \label{fig:smallinplane}
  \vspace{-10pt}
\end{wrapfigure}

\begin{figure}[tb]
\includegraphics[width=\textwidth]{figures/axial.pdf}
\caption{A schematic view of the axial and praxial \rasnik alignment systems 
    along six
barrel \msmdt chambers. Axial lines span two full chambers, while praxial lines
only span the gap between two chambers.}
\label{fig:praxial}
\end{figure}


\smallskip
The \emph{projective} \rasnik systems connect the three layers of a sector.
The mask is mounted on a chamber of the inner layer, while the lens and
the sensor are mounted on chambers of the middle and outer layers respectively.
The extrapolation of the optical axis of each projective line intersects
with the interaction point, such that high-\pt{} muon tracks are parallel to
these lines, maximizing their resolution in the relevant direction.

Projective systems are only mounted on the odd-numbered sectors, since the
need to go through the middle layer of adjacent sectors complicates the design
(see Figure~\ref{fig:barrelalign}).

\smallskip
The even-numbered sectors are aligned with respect to the odd-numbered
sections via \emph{chamber-to-chamber connections} ({\sc ccc}s). These consist
of a \mssacam cameras mounted on the on the even-numbered sectors looking at
target spots on the odd-numbered sectors. Three {\sc ccc}s on each side
connect the inner and middle layers to the adjacent sectors, while only
two {\sc ccc}s per side do so for the outer layer.

\smallskip

An additional \emph{reference} system of \mssacam{}s measures the position
of the inner and middle layer chambers in odd-numbered sectors with respect
to the barrel toroid in order to correct for higher order displacements.
This reference system also monitors the position and deformation of the
toroid coils with systems installed between to points on one coil or
from coil to coil.

\subsection{Endcap alignment}

The muon spectrometer endcap consists of three \emph{wheels}, disks covered
with muon chambers. On each side, the 
inner wheel is mounted between the barrel toroid
magnet and the endcap toroid magnet cryostat. The middle and outer wheels
are mounted behind the endcap magnet. 


As the endcap magnet cryostats are placed in between the inner and middle wheels
projective lines like in the barrel can not be used in the endcaps.
Instead, reference bars are aligned with bar-to-bar alignment systems,
and chambers are aligned to these bars with chamber-to-bar alignment systems.

These reference bars are mounted throughout each wheel. 
The deformation of each alignment
bar is monitored by in-bar \rasnik systems, and the positions of the bars
with respect to eachother is measured by long-distance \msbcam systems.

Similar to the alignment in the barrel, each \msmdt chamber in the endcap
has four \emph{inplane} \rasnik systems: two parallel to the tubes and
two diagonally. The deformation of the \mscsc{}s is only measured prior to
installation in the \atlas detector.
Proximity systems (\rasnik{s}) monitor the position of the \msmdt chambers
and \mscsc{s} with respect to eachother and to the alignment bars.

\subsection{Alignment performance}

Figure~\ref{fig:alreso} shows the performance of the barrel alignment system.
The distinction between odd sectors (with projective 
alignment system) and the even sectors (without) is clearly visible,
    as the resolution in the
latter is half an order of magnitude worse. Nevertheless, also in the even
sectors the resolution is close to the requirement 
of~$50\unit{\upmu m}$~\cite{pfalign}.

\begin{figure}[tb]

\includegraphics[width=\textwidth,trim=0 7cm 0 6cm,clip]{figures/resolution.pdf}
\caption{Resolution of the optical barrel alignment system. 
{\atlas work in progress~\cite{pfalign}.}}
\label{fig:alreso}
\end{figure}

The
resolution of the alignment in the endcaps is, due to the much larger distances,
not as good as in the barrel. Where the average resolution in the barrel
in $43\unit{\upmu m}$, it is $90\unit{\upmu m}$ in the even sectors
of the endcaps. Fortunately, this is not too far from the requirement
to pose a large problem.
Current work focuses on introducing nuisance parameters in the track fit
to account for the misalignment of the muon chambers.
The results of the track fitter on data can then 
be used to determine the alignment of the muon chambers.


\section{Trigger}
\label{sec:trigger}
The \atlas
trigger system is designed to reduce the event rate from the interaction
rate of $1\unit{GHz}$ to a rate of the order of $100\unit{Hz}$, 
     which can be written to disk. 
It is divided into three levels: 
the \emph{level-1 trigger}~\cite{triggerTDR},
the \emph{level-2 trigger} and the \emph{event filter}~\cite{Jenni:616089}.
Together the level-2 trigger and the event filter make up the 
\emph{high level trigger} (\hlt), which make a more refined selection of the 
events that are selected by the level-1 trigger.


The level-1 trigger is hardware-based, which allows a much higher event rate
but severely limits the number and complexity of the calculations which can
be performed. Therefore only information from the calorimeter and the trigger
chambers in the muon spectrometer is used. 
Objects triggered on by the level-1 trigger include high-\pt{} electrons, muons
and photons,~large \met{} and total transverse energy.

As shown in Figure~\ref{fig:trigger} the events that are selected by the level-1
trigger are read into the \emph{readout buffers} ({\sc rob}s) by 
\emph{readout drivers} ({\sc rod}s). The \emph{derandomizers} placed after the
level-1 trigger average out the incoming 
data rate to match the
input rate of the {\sc rod}s.
\begin{figure}[tb]
\includegraphics[width=\textwidth]{figures/trigger.pdf}
\caption{A block diagram of the data flow in the \atlas trigger.}
\label{fig:trigger}
\end{figure}

Event data is stored in the {\sc rob}s until the software-based level-2 trigger
rejects the event or after it has been sent to the \emph{event builder} for
further processing. The level-1 trigger provides the level-2 trigger with
a \emph{region of interest} for each event. This includes information on 
high-\pt{} regions and~\met. By using these regions of interest, it does not
need to access the full event data.

The event filter has access to the complete selected events, and has sufficient
calculation time to select events based on complex algorithms. It is designed
to reduce the input event rate of the order of $1\unit{kHz}$ to a rate which
can be written to disk, which is about~$400\unit{Hz}$ for events
of roughly~$1\unit{MB}$ size.

\smallskip
The trigger uses a  complex set (\emph{menu}) of signatures, 
 designed to identify objects, such as electrons, muons and
jets, and the missing transverse energy, to select events based on the 
properties of these objects. Triggers can be prescaled in order to reduce their
footprint in the total event rate. The events used in the analyses in this 
thesis, single top~$t$-channel and~$Wt$-channel events with one lepton,
are selected by a trigger for isolated leptons. The trigger rate for true
single top~$t$-channel and~$Wt$-channel events with one lepton was about~2
per minute in~2012.


%*****************************************
%*****************************************
%*****************************************
%*****************************************
%*****************************************
