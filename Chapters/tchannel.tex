%************************************************
\chapter{Single top $t$-channel}\label{ch:tchan} 
%************************************************

%This chapter describes the first part of an analysis in $t$-channel single top.
This analysis focuses on the leptonic 2-jet signature of the $t$-channel,
where the top quark decays leptonically, i.e.\ to a bottom quark and a
$W$ boson which subsequently decays to a lepton and a neutrino, and where
both the bottom quark from the top decay as well as the light quark from
the $W$ interaction are detected (see figure~\ref{fig:tchandiag2}).

\begin{figure}[h]
  \centering
  \includegraphics[scale=0.8]{figures/tchB}
  \caption{Leading order t-channel production Feynman diagram.}
  \label{fig:tchandiag2}
\end{figure}

The main backgrounds to the leptonic $t$-channel are top pair and $W$+jets. 
Other backgrounds are $s$-channel and $Wt$-channel single top production, 
$Z$+jets, diboson and \qcd multijet.

\smallskip
This chapter describes the selection of the $t$-channel events and the 
measurement of the cross section using a \emph{cut and count} method. The
results described in this chapter are used in a more detailed $t$-channel
analysis described in the next chapter, which focuses on polarization.

\section{Data and monte carlo}

This analysis is performed using the 2012 dataset recorded by \atlas
at a center-of-mass energy of~$\sqrt{s} = 8\unit{TeV}/c^2$
with an integrated luminosity of~$20.28\unit{fb}^{-1}$.
The contribution from the \qcd multijet background is determined using the
data-driven matrix method described in section~\ref{sec:bkgest} 
and~\cite{ATLAS-CONF-2014-058}.
Other contributions from background and
signal processes are estimated using \montecarlo samples,
in some cases normalized using data.


The $t$-channel signal \montecarlo sample is generated 
%at leading order
using \acermc and \pythia. 
%While it would in principle be better to use a next-to-leading order
%generator this is not possible in the second part of this analysis,
%as explained in the next chapter.
\acermc is a leading-order generator, which allows for an unambiguous 
identification of the truth-level partons participating in the hard scatter
interaction. This is essential in the analysis described in the next chapter.

All top-quark backgrounds, $s$-channel, $Wt$-channel and top pair,
are generated using Powheg and \pythia. Sherpa is used for both
$W$+jets and $Z$+jets, and the diboson samples are generated using \herwig.

The $W$+jets background is the main background in this analysis. Since
Sherpa is a leading-order generator the normalization of the $W$+jets
background prediction is not very accurate and has to be determined with
data.
%As Sherpa does not generate separate $W$+jets samples categorized by 
%the flavor of the quarks in the matrix element, the Sherpa samples were
%split up into three samples depending on the flavor of the final state quarks,
%replacing the categorization as done by Alpgen (see section~\ref{sec:bkgest}):
%one with bottom quarks~($W_{b}$), one with charm 
%quarks~(and no bottom quarks)~($W_c$) and one with only light quarks~($W_{l}$).
The Sherpa samples were split up into three samples depending on the flavor
of the final state quarks, as Sherpa does not generate separate $W$+jets samples
categorized by the flavor of the quarks in the matrix element. This
    replaces the categorization as done by Alpgen
(see section~\ref{sec:bkgest}). The three samples are:
one with bottom quarks~($W_{b}$), one with charm
quarks~(and no bottom quarks)~($W_c$) and one with only light quarks~($W_{l}$).

The resulting correction factors are shown in table~\ref{tab:tchff}.
These factors are determined using the tag-counting method described
in section~\ref{sec:bprime-wjets}, where events from the final selection
are removed from the used preselection tag and pretag samples in order to
reduce the effect of the signal cross section on these factors.
%The resulting correction factors, which are determined using the
%tag-counting method described in section~\ref{sec:bprime-wjets}, 
%    are shown in table~\ref{tab:tchff}. 
As can be seen,
    these factors are strongly correlated between jet-bins.
A significant difference can be seen between the correction factors of
the electron and the muon channels. 
    
%    The differences between the
%    correction factors for the electron and muon channels are so far 
%    unexplained, but are seen by many other analyses.
    
\begin{table}[htp]
\centering
\begin{subtable}{0.48\textwidth}
\centering
\begin{tabular}{ c | c c c c }
jet-bin & $\kappa_{b}$ & $\kappa_{c}$ & $\kappa_{l}$ & $\kappa_{\mathrm{tot}}$ \\\hline
1 & 0.87 & 0.79 & 1.05 & 0.93 \\
2 & 0.89 & 0.81 & 1.07 & 0.99 \\
3 & 0.91 & 0.83 & 1.09 & 0.92 \\
4 & 0.92 & 0.84 & 1.11 & 0.91 \\
\end{tabular}
\caption{Electron channel.}
\end{subtable}
\begin{subtable}{0.48\textwidth}
\centering
\begin{tabular}{ c | c c c c }
jet-bin & $\kappa_{b}$ & $\kappa_{c}$ & $\kappa_{l}$ & $\kappa_\mathrm{tot}$ \\\hline
1 & 0.90 & 1.16 & 0.97 & 1.05 \\
2 & 0.89 & 1.15 & 0.96 & 1.14 \\
3 & 0.88 & 1.14 & 0.95 & 1.04 \\
4 & 0.88 & 1.13 & 0.95 & 0.96 \\
\end{tabular}
\caption{Muon channel.}
\end{subtable}
\caption{Correction factors for heavy flavor composition
    ($\kappa_b$, $\kappa_c$, $\kappa_l$) 
  and overall normalization ($\kappa_{\mathrm{tot}}$) for Sherpa 
      $W$+jets as determined with the
tag counting method. 
The uncertainty on these correction factors when applied to the final
selection is $50\%$ for the flavor composition factors and $20\%$ for the
overall normalization~\cite{ATLAS-CONF-2012-131}.
}
\label{tab:tchff}
\end{table}



\section{Event selection}

%This section describes the event selection that is used in this analysis.
The selection consists of two stages: the \emph{preselection} and the 
\emph{selection}. The preselection selects events with a basic $t$-channel
signature. After the preselection the top quark can be reconstructed.
The event selection is designed to 
efficiently select~$t$-channel events while
increasing the ratio
of signal to background.
%to reduce the backgrounds that are difficult to model,
%and to only pass events which have a leptonic $t$-channel signature.

\subsection{Preselection}

The event preselection of this analysis follows a similar strategy as
the event preselection of
the \atlas~$7\unit{TeV}$ $t$-channel cross section 
measurement~\cite{Aad:1448641}. Events are required to contain at least
one good primary vertex candidate and to pass several data quality requirements.
Exactly one lepton (electron or muon) is required, which must match the
trigger-level lepton. 

The missing transverse energy (\met{})
    must be larger than~$30\unit{GeV}$, and in order
to reduce the amount of \qcd multijet background,
%which is difficult to model~\cite{Aad:2010ey}, 
    the transverse $W$ mass\footnote{See 
    section~\ref{sec:bpevtsel} for a definition.} is required to be larger
than~$50\unit{GeV}/c^2$. These requirements were tightened with respect to 
the~$7\unit{TeV}$ $t$-channel measurement.
%since this analysis is performed on~$8\unit{TeV}$ data. 
Finally, each event is required to have exactly two
jets, of which one is $b$-tagged.

In summary, the most important preselection cuts are:
\begin{align}
  \MET &> 30\unit{GeV}, \label{eq:precut-met} \\
  n_\mathrm{jets} &= 2, \label{eq:precut-jets} \\
  \MTW &> 50\unit{GeV}/c^2, \label{eq:precut-mtw} \\
  n_\mathrm{b\-- jets} &= 1. \label{eq:precut-bjets}
\end{align}
Table~\ref{tab:tchpresel} shows the event yields for the electron and muon 
channels after preselection, with (\emph{tag}) and without (\emph{pretag})
the $b$-tagging requirement (equation~\ref{eq:precut-bjets}). 

%The \montecarlo
%event yields and plots shown in this chapter has been scaled using
%scale factors determined by a likelihood fit as described in 
%section~\ref{sec:likfit} and shown in table~\ref{tab:tchsf}. 

\begin{table}[tp]
  \centering
  \input{tables/presel-sn}
  \caption{Event yields after the preselection cuts. 
      All quoted uncertainties
  are purely based on \mc statistics, except for the \qcd multijet uncertainty
  which is set to $50\%$.}
  \label{tab:tchpresel}
\end{table}

%\begin{table}[htp]
%  \centering
%  \begin{tabular}{ r  c c c }
%      & $t$-channel & other top & $W$+HF jets \\\hline
%    electron & 1.13 & 0.99 & 0.92 \\
%    muon & 1.15 & 1.06 & 0.87 \\
%  \end{tabular}
%  \caption{Scale factors for the signal and main background \montecarlo samples,
%  as determined in section~\ref{sec:likfit}.}
%  \label{tab:tchsf}
%\end{table}

\begin{figure}[p]
  \hfill
  \begin{subfigure}{0.49\textwidth}
    \centering
      \includegraphics[width=\textwidth]{plots/nom_el_2j_lpt_pp}
    \vskip -2mm 
    \caption{Electron \pt, pretag.}
  \end{subfigure}
  \hfill
  \begin{subfigure}{0.49\textwidth}
    \centering
      \includegraphics[width=\textwidth]{plots/nom_el_2j_lpt_pt}
    \vskip -2mm 
    \caption{Electron \pt, tag.}
  \end{subfigure}
  \hfill\\\vskip 2mm
  \hfill
  \begin{subfigure}{0.49\textwidth}
    \centering
      \includegraphics[width=\textwidth]{plots/nom_el_2j_MET_pp}
    \vskip -2mm 
    \caption{\met, pretag.}
  \end{subfigure}
  \hfill
  \begin{subfigure}{0.49\textwidth}
    \centering
      \includegraphics[width=\textwidth]{plots/nom_el_2j_MET_pt}
    \vskip -2mm 
    \caption{\met, tag.}
  \end{subfigure}
  \hfill\\\vskip 2mm
  \hfill
  \begin{subfigure}{0.49\textwidth}
    \centering
      \includegraphics[width=\textwidth]{plots/nom_el_2j_MTW_pp}
    \vskip -2mm 
    \caption{\MTW, pretag.}
  \end{subfigure}
  \hfill
  \begin{subfigure}{0.49\textwidth}
    \centering
     \includegraphics[width=\textwidth]{plots/nom_el_2j_MTW_pt}
    \vskip -2mm 
    \caption{\MTW, tag.}
  \end{subfigure}
  \hfill\\\vskip 2mm

  \hfill\\
  \caption{Electron channel preselection control plots. 
      The plots on
  the left side show pretag distributions, while the plots on the right side
  show tag distributions. The hatched band on the \montecarlo and the unity
  line in the ratio plot is the statistical \montecarlo uncertainty.}
  \label{fig:tchpresel}
\end{figure}


\begin{figure}[p]
  \ContinuedFloat
  \hfill
  \begin{subfigure}{0.49\textwidth}
    \centering
      \includegraphics[width=\textwidth]{plots/nom_mu_2j_lpt_pp}
    \vskip -2mm 
    \caption{Muon \pt, pretag.}
  \end{subfigure}
  \hfill
  \begin{subfigure}{0.49\textwidth}
    \centering
      \includegraphics[width=\textwidth]{plots/nom_mu_2j_lpt_pt}
    \vskip -2mm 
    \caption{Muon \pt, tag.}
  \end{subfigure}
  \hfill\\\vskip 2mm
  \hfill
  \begin{subfigure}{0.49\textwidth}
    \centering
      \includegraphics[width=\textwidth]{plots/nom_mu_2j_MET_pp}
    \vskip -2mm 
    \caption{\met, pretag.}
  \end{subfigure}
  \hfill
  \begin{subfigure}{0.49\textwidth}
    \centering
      \includegraphics[width=\textwidth]{plots/nom_mu_2j_MET_pt}
    \vskip -2mm 
    \caption{\met, tag.}
  \end{subfigure}
  \hfill\\\vskip 2mm
  \hfill
  \begin{subfigure}{0.49\textwidth}
    \centering
      \includegraphics[width=\textwidth]{plots/nom_mu_2j_MTW_pp}
    \vskip -2mm 
    \caption{\MTW, pretag.}
  \end{subfigure}
  \hfill
  \begin{subfigure}{0.49\textwidth}
    \centering
     \includegraphics[width=\textwidth]{plots/nom_mu_2j_MTW_pt}
    \vskip -2mm 
    \caption{\MTW, tag.}
  \end{subfigure}
  \hfill\\\vskip 2mm

  \hfill\\
  \caption{Muon channel preselection control plots.
      The plots on
  the left side show pretag distributions, while the plots on the right side
  show tag distributions. The hatched band on the \montecarlo and the unity
  line in the ratio plot is the statistical \montecarlo uncertainty.}
  \label{fig:tchpresel}
\end{figure}

Plots showing the distributions at pretag and tag level
of three key variables, the lepton transverse
momentum, the missing transverse energy and the transverse $W$ mass, for
the electron and muon channels are shown in figure~\ref{fig:tchpresel}.
The distributions
show a good data-\montecarlo agreement, although the muon channels shows a
small discrepancy before the $b$-tagging requirement.
In general, the enormous impact of 
this $b$-tagging requirement on the backgrounds is cleary visible.


\subsection{Particle reconstruction}

Each event that passes the preselection contains the decay products of the
leptonic decay of a top quark: a lepton (electron or muon), a neutrino
in the form of missing transverse energy
and a heavy jet. From these decay products we can
reconstruct the four-momenta of the $W$ boson and the top quark.

\paragraph{W boson reconstruction}

The momentum of the lepton (electron or muon) is measured by the detector,
  but the neutrino escapes all detection. By assuming that the neutrino
  is the only particle that is not detected, and therefore the only particle
  that contributes to the missing transverse energy, we can write down an
  equation to reconstruct the neutrino four-momentum using the W boson 
  mass of $M(W) = 80.399\unit{GeV}/c^2$:
\begin{equation}
  M^2(W) = M^2(\ell) + 
  \frac{2}{c^2}\left(\frac{E(\ell)E(\nu)}{c^2}
          -\vec{p}(\ell)\cdot\vec{p}(\nu)\right), 
\end{equation}
where $M(\ell)$ is the mass of the charged lepton, $E(\ell)$ and~$E(\nu)$ 
are the energies of the charged lepton and the neutrino 
respectively,~$\vec{p}(\ell)$
and~$\vec{p}(\nu)$ are the momenta of the charged lepton and the 
neutrino respectively, and the neutrino mass is assumed to be zero.
We can write this as a quadratic equation in $p_z(\nu)$:
\begin{equation}
  \begin{split}
 0 &= \left(p_z^2(\ell)c^2 - E^2(\ell)\right)\,p_z^2(\nu)c^2\\
   &+ \left(\Delta M_2c^2 + 2cp_T(\ell)\cdot\MET\right)
       \,p_z(\ell)\,p_z(\nu)c^2\\
   &+ \frac{(\Delta M_2)^2c^4}{4} + 
        \Delta M_2\left(p_T(\ell)\cdot\MET\right)c^3
        + \left(p_T(\ell)\cdot\MET\right)^2c^2,
\label{eq:quadrecoW}
\end{split}\end{equation}
where $\Delta M_2 = M^2(W)-M^2(\ell)$, and $p_x + p_y = p_T$. Depending on the
momentum of the lepton and the missing transverse energy, 
equation~\ref{eq:quadrecoW} may have 0, 1 or 2 real solutions for~$p_z(\nu)$.
The solution with the lowest~$|p_z(\nu)|$ is used in the case that there
are two real solutions.
When there are no real solutions the \MET{} is scaled down to the value
at which the equation has exactly one real solution.

\paragraph{Top quark reconstruction}

The top quark decays to a W boson and a bottom quark. By adding the 
momentum of the
bottom quark as measured by the detector and the momentum of the W boson,
reconstructed as described above, we obtain the 
reconstructed momentum of the top quark. 
This yields distributions as shown in figures~\ref{fig:cp.el.topmass} 
and~\ref{fig:cp.mu.topmass}. The other subfigures in figure~\ref{fig:tchsel}
are described below.
%In the case that the W boson
%reconstruction yields more than one solution, the solution which results in
%a reconstructed top quark with a mass closest to $m_{top} = 172.5\unit{GeV}$ is
%used. The reconstructed top quark mass is therefore biased toward this value,
%    but since we do not measure the top quark mass in this analysis

\subsection{Selection}

In order to further reduce the remaining backgrounds cuts are placed on four
more variables. These cuts are placed
on the absolute pseudorapidity of the light 
(non-$b$-tagged) jet, the scalar sum of all particle transverse momenta and 
the missing transverse energy (\HT), the reconstructed top mass as described
above and the absolute difference in pseudorapidity between the two jets.

\begin{figure}[p]
  \hfill
  \begin{subfigure}{0.49\textwidth}
    \centering
    \begin{tikzpicture}
      \node{\includegraphics[width=\textwidth]{plots/nom_el_2j_etalj_abt}};
      \fill[white,opacity=0.6](-0.19,0.89) rectangle (-2.0,1.35);
      \fill[white,opacity=0.6](-2.250,-1.06) rectangle (-0.19,0.89);
    \end{tikzpicture}
    \vskip -2mm 
    \caption{Electron channel, $|\eta(j)|$.}
  \end{subfigure}
  \hfill
  \begin{subfigure}{0.49\textwidth}
    \centering
    \begin{tikzpicture}
      \node{\includegraphics[width=\textwidth]{plots/nom_mu_2j_etalj_abt}};
      \fill[white,opacity=0.6](-0.19,1.05) rectangle (-2.0,1.30);
      \fill[white,opacity=0.6](-2.250,-1.06) rectangle (-0.19,1.05);
    \end{tikzpicture}
    \vskip -2mm 
    \caption{Muon channel, $|\eta(j)|$.}
  \end{subfigure}
  \hfill\\\vskip 2mm

  \hfill
  \begin{subfigure}{0.49\textwidth}
    \centering
    \begin{tikzpicture}
      \node{\includegraphics[width=\textwidth]{plots/nom_el_2j_ht_abt}};
      \fill[white,opacity=0.6](-0.45,-1.06) rectangle (-1.22,1.25);
    \end{tikzpicture}
    \vskip -2mm 
    \caption{Electron channel, $\HT$.}
  \end{subfigure}
  \hfill
  \begin{subfigure}{0.49\textwidth}
    \centering
    \begin{tikzpicture}
      \node{\includegraphics[width=\textwidth]{plots/nom_mu_2j_ht_abt}};
      \fill[white,opacity=0.6](-0.45,-1.06) rectangle (-1.22,1.25);
    \end{tikzpicture}
    \vskip -2mm 
    \caption{Muon channel, $\HT$.}
  \end{subfigure}
  \hfill\\\vskip 2mm

  \hfill
  \begin{subfigure}{0.49\textwidth}
    \centering
    \begin{tikzpicture}
      \node{\includegraphics[width=\textwidth]{plots/nom_el_2j_top_abt}};
      \fill[white,opacity=0.6](-0.30,-1.06) rectangle (2.6,1.05);
      \fill[white,opacity=0.6](2.89,-1.06) rectangle (2.6,-0.99);
      \fill[white,opacity=0.6](-0.71,-1.06) rectangle (-1.33,1.05);
    \end{tikzpicture}
    \vskip -2mm 
    \caption{Electron channel, $m_{top}$.}
    \label{fig:cp.el.topmass}
  \end{subfigure}
  \hfill
  \begin{subfigure}{0.49\textwidth}
    \centering
    \begin{tikzpicture}
      \node{\includegraphics[width=\textwidth]{plots/nom_mu_2j_top_abt}};
      \fill[white,opacity=0.6](-0.30,-1.06) rectangle (2.6,1.05);
      \fill[white,opacity=0.6](2.89,-1.06) rectangle (2.6,-0.99);
      \fill[white,opacity=0.6](-0.71,-1.06) rectangle (-1.33,1.05);
    \end{tikzpicture}
    \vskip -2mm 
    \caption{Muon channel, $m_{top}$.}
    \label{fig:cp.mu.topmass}
  \end{subfigure}
  \hfill\\
  \caption{Distributions of the quantities used in the selection cuts after
  all selection cuts except the one on the concerning variable. The regions
  removed by the cut on the concerning variable is shown shaded.}
  \label{fig:tchsel}
\end{figure}


\begin{figure}[htp]
  \ContinuedFloat
  \hfill
  \begin{subfigure}{0.49\textwidth}
    \centering
    \begin{tikzpicture}
      \node{\includegraphics[width=\textwidth]{plots/nom_el_2j_deltaeta_abt}};
      \fill[white,opacity=0.6](-1.39,-1.06) rectangle (-2.06,1.3);
      \fill[white,opacity=0.6](-2.25,-1.06) rectangle (-2.06,-0.55);
    \end{tikzpicture}
    \caption{Electron channel, $\Delta\eta(j,b)$.}
  \end{subfigure}
  \hfill
  \begin{subfigure}{0.49\textwidth}
    \centering
    \begin{tikzpicture}
      \node{\includegraphics[width=\textwidth]{plots/nom_mu_2j_deltaeta_abt}};
      \fill[white,opacity=0.6](-1.39,-1.06) rectangle (-2.06,1.3);
      \fill[white,opacity=0.6](-2.25,-1.06) rectangle (-2.06,-0.4);
    \end{tikzpicture}
    \caption{Muon channel, $\Delta\eta(j,b)$.}
  \end{subfigure}
  \hfill\\
  \caption{Distributions of the quantities used in the selection cuts after
  all selection cuts except the one on the concerning variable. The regions
  removed by the cut on the concerning variable is shown shaded.}
  \label{fig:tchsel}
\end{figure}

\begin{table}[tp]
  \centering
  \input{tables/sel}
  \caption{Event yields after the selection cuts. 
      All quoted uncertainties
  are purely based on \mc statistics, except for the \qcd multijet uncertainty
  which is set to $50\%$.}
  \label{tab:tchsel}
\end{table}

The cuts are:
\begin{align}
  |\eta(j)| &> 2  \label{eq:cut1}, \\
  \HT &> 210\unit{GeV} \label{eq:cut2}, \\
  150 < m_\mathrm{top} &< 190\unit{GeV}/c^2 \label{eq:cut3}, \\
  |\Delta\eta(b,j)| &> 1 \label{eq:cut4}.
\end{align}
Figure~\ref{fig:tchsel} shows the distributions of the four variables used
in these cuts after all selection cuts including the effect of the concerning
cut. Table~\ref{tab:tchsel} shows the event yields after the selection.

The cut on the pseudorapidity of the light jet makes use of the fact that
in $t$-channel this jet is rather forward (high absolute pseudorapidity) and
is effective against the $W$+jets and top backgrounds. The cut on \HT{}
makes use
of the high transverse momenta of the top decay products, and hence is effective
against all non-top backgrounds. 
The cut on the reconstructed top mass is, naturally, effective against all
backgrounds in which there is no top quark to reconstruct. 

The distribution of the variable used in the last cut, the 
absolute difference in pseudorapidity between the jets, shows some disagreement
between data and \montecarlo in the region $|\Delta\eta(b,j)| > 3$.
Therefore the cut was placed at a low value, away from this region, making
it only moderately effective against the remaining backgrounds. It might be
possible to make it more effective by cutting at a higher value, but this
comes with the risk of introducing a discrepancy between data and \montecarlo.

\begin{table}[htp]
  \centering
  \begin{tabular}{r |  c c c }
    & \multicolumn{2}{c}{preselection} & selection \\
    & pretag & tag & \\ \hline
  electron & $1.4\cdot10^{-2}$ & $1.8\cdot10^{-1}$ & $1.2$ \\
  muon     & $1.3\cdot10^{-2}$ & $1.7\cdot10^{-1}$ & $1.1$ \\
  combined & $1.4\cdot10^{-2}$ & $1.7\cdot10^{-1}$ & $1.2$ \\
  \end{tabular}
  \caption{Signal-to-background ratio at each step in the event selection.
      The ratio increases by about an order of magnitude at each step.}
  \label{tab:s2b}
\end{table}

\bigskip
The preselection only selects events based on quality requirements
and basic event topology and provides a signal-to-background ratio
of the order of about $1/75$ before the $b$-tagging requirement and
$1/5$ after the $b$-tagging requirement. The selection 
is efficient for signal and improves the ratio
by about an order of magnitude, to $1.2$ (see table~\ref{tab:s2b}).
%   without reducing the number of
%remaining events to a problematically low value.

\section{Cross section measurement}
\label{sec:tchcut}

With the events selected as above the
$t$-channel production cross section can be measured by scaling
the $t$-channel sample such that data and \mc match. In practice,
in order to aid calculating the uncertainties, this is done by
using a maximum-likelihood fit on the total number of signal and background
events:
\begin{equation}
  P(N_{obs}\,|\,\mu N_s + N_b),
\end{equation}
where~$N_{obs}$ is the number of observed events,~$N_s$ and $N_b$ are  the 
number of expected signal and background events respectively, and~$\mu$ is a
free parameter. RooFit~\cite{Verkerke:622147}
is used to perform the maximum-likelihood fit.


When combining electron and muon channels,
     $\mu$ is found to be $\mu = 1.14 \pm 0.025$,
where the uncertainty is based only on data statistics. Since the cross section
used for generating the signal \montecarlo sample 
is~$\sigma_{mc} = 87.76\unit{pb}$,
we obtain 
\begin{align}
  \sigma_{t\mathrm{-channel}} &= 100.1 \pm 3.34 \unit{pb}\,\,\mathrm{(electron)},\\
  \sigma_{t\mathrm{-channel}} &= \phantom{1}99.8 \pm 3.30 \unit{pb}\,\,\mathrm{(muon)},\\
  \sigma_{t\mathrm{-channel}} &= 100.1 \pm 2.22 \unit{pb}\,\,\mathrm{(combined)}.
\end{align}
The uncertainties quoted here are based purely on data statistics.

\section{Systematic uncertainties}
\label{sec:tchsys}

In order to estimate the systematic uncertainty of the cross section
measurement described
in the previous section, various contributions to the total 
systematic uncertainty are evaluated. Each of these systematic uncertainties
is briefly described below.

\paragraph{Luminosity}
The uncertainty on the total integrated luminosity in the 2012 dataset is
estimated using the same method as used for the 2011 dataset as described 
in~\cite{Aad:2013ucp}. The uncertainty on the luminosity is $\pm2.8\%$.

\paragraph{Jet energy scale and resolution}
The uncertainty on the jet energy scale is a large contributor to the 
systematic uncertainty. It is evaluated by shifting the energies of simulated
jets up and down by one standard deviation (depending on the region of the
detector and the energy of the jet, this varies between~$1$ 
and~$6\%$~\cite{Aad:2014bia}), 
     and successively performing the
analysis including object and event selection. 
The jet energy resolution uncertainty is evaluated by smearing the resolution
in simulation.

\paragraph{Jet reconstruction efficiency}
The calorimeter jet reconstruction efficiency is measured with respect to
reconstructed tracks of 
probe jets. The uncertainty on this efficiency is evaluated by varying
the \montecarlo jet reconstruction efficiency.

\paragraph{$B$-tagging}
The uncertainties on the $c$-tag, $b$-tag and mistag rates are evaluated by
varying the scalefactors used in \montecarlo for each of these rates one at
a time. While the uncertainties on the $b$-tag rates are of the order of~$2$
to~$8\%$ depending on the energy of the jet,
    the $c$-tag and mistag rate uncertainties are much larger at~$8$
to~$15\%$ and~$15$ to~$40\%$ respectively~\cite{ATLAS-CONF-2014-046,ATLAS-CONF-2014-004}.


\paragraph{Lepton energy scale, resolution and reconstruction efficiency}
The uncertainties on the lepton scale, resolution and reconstruction 
efficiency are evaluated like the corresponding uncertainties for the jets.
These uncertainties are evaluated separately for each lepton flavor,
and are much smaller than the jet uncertainties~\cite{Acharya:1563201}.

\paragraph{Missing transverse energy scale and resolution}
The effects of the jet and lepton variations are taken into account when
calculating the \met{} while evaluating these uncertainties, so these effects
do not need to be evaluated separately. The effects of the scale and resolution
of the measurement of soft jets which are not included in the jet uncertainties
are evaluated by separately varying the soft jet scale and resolution and 
recalculating the \met. These uncertainties are of the order 
of~$2$ to~$3\%$~\cite{ATLAS-CONF-2013-082}.

\paragraph{mc generator} Often there are differences in the results produced
by different \montecarlo generation approaches. These differences are evaluated
by replacing one of the \montecarlo samples by one generated with a different
generator. This is done for all main backgrounds and the signal.

\begin{itemize}
\item[]\textsc{Top pair} The nominal top pair sample is generated by
Powheg and \pythia. This sample is compared to a  sample generated by \mcatnlo.
\item[]\textsc{$W$+jets} The nominal $W$+jets sample is generated by
Sherpa. An alternative sample generated by Alpgen is used to compare to.
The Z+jets background, of which the default sample is also generated by
Sherpa, is also compared to a sample generated by Alpgen, simultaneously
with the $W$+jets background.
\item[]\textsc{$t$-channel} The nominal $t$-channel signal sample is
generated by \acermc, which is a leading order generator. Several 
next-to-leading order generators are available, of which we use Powheg
to compare to. As explained in the next chapter, it is not possible to
use Powheg as the nominal generator. The cross section that is used to
normalize the two samples is the same\footnote{This is the result of
    a next-to leading order
    plus next-to-next-to leading log calculation~\cite{Kidonakis:2011wy}.}: 
    $87.76\unit{pb}$.
\end{itemize}

\paragraph{Background cross section}
The uncertainties on the cross section of each background are incorporated
by varying each background up and down by a predetermined fraction. The 
variations are shown in table~\ref{tab:tchxsecsys}. 

The uncertainties on the top backgrounds are theoretical 
uncertainties~\cite{Kidonakis:2012db,ATLAS-CONF-2014-007}.
%The uncertainty found 
%using a data-driven method in
%the~$\sqrt{s} = 7\unit{TeV}$ top pair cross section 
%measurement~\cite{ATLAS-CONF-2012-131} is used as uncertainty for the
%$W$+jets normalization. 
The uncertainty found using a data-driven method in 
the~$\sqrt{s} = 7\unit{TeV}$~$t$-channel cross section 
measurement~\cite{Aad:1448641} is used
for the~$W$+jets normalization. In this uncertainty, 
    the effects of all other systematic
uncertainties on the~$W$+jets normalization have been taken into account.
This includes a~$100\%$ uncertainty on the~$t$-channel cross section.
The theory uncertainties on the $Z$+jets and diboson
cross sections are $4\%$ and $5\%$ respectively. In both cases an uncertainty
due to Berends scaling of $24\%$ has to be included~\cite{Acharya:1298781}.
The systematic uncertainties
on the cross sections are evaluated separately for each background. 

\begin{table}[htp]
  \centering
  \begin{tabular}{ r | c c }
  & up & down \\ \hline
  top pair     & $+5.9\%$ & $-5.9\%$ \\
  $Wt$-channel & $+6.8\%$ & $-6.8\%$ \\
  $s$-channel  & $+3.9\%$ & $-3.9\%$ \\
  $W$+jets     & $+28\%$ & $-28\%$ \\
  $Z$+jets     & $+24\%$ & $-24\%$ \\
  diboson      & $+25\%$ & $-25\%$ \\
  \end{tabular}
  \caption{Uncertainties on the background cross sections. All uncertainties
  are theory uncertainties, except for the uncertainty on $W$+jets.}
  \label{tab:tchxsecsys}
\end{table}

\paragraph{qcd multijet normalization}
Like in the analysis described in the previous chapter, a normalization
uncertainty of $50\%$ is assigned to the \qcd multijet background.

%\paragraph{$W$+jets scale factors} The effect of the uncertainty on the
%$W$+jets scale factors as shown in table~\ref{tab:tchff} is evaluated by
%varying~$\kappa_c$ by~$50\%$ while keeping the total normalization of
%the~$W$+jets after the preselection tag cut equal. 

\paragraph{PDF}
As shown in~\cite{ATLAS-CONF-2014-007}, the uncertainty due to the \pdf{}s is $\pm1.1\%$.

\section{Results}
\label{sec:tchanresults}
Object and event selections are performed for each of the abovementioned
systematic uncertainties. 
Uncertainties which do not have individual up and down variations are 
symmetrized by taking the nominal with the negative difference as opposite
variation. The effects of the systematic uncertainties 
are shown in table~\ref{tab:sysyield}. The change from \acermc to Powheg
as $t$-channel generator gives by far the largest uncertainty.

\begin{table}[htp]
  \centering
  \input{tables/csys.tex}
  \caption{Effects of each systematic uncertainty on the measured cross section.
      Uncertainties without individual up and down variations are
  symmetrized.}
  \label{tab:sysyield}
\end{table}
\bigskip

%The \montecarlo statistical uncertainty and  the systematic uncertainty
%on the cross section measurement are calculated using standard error prop
%
%calculated by performing 
%a maximum-likelihood fit with a gaussian constraint for each
%uncertainty using RooFit~\cite{Verkerke:622147}. Again the electron and muon
%channels are combined.



The large difference in signal event yields between the \acermc and Powheg
samples is unfortunate, but not unexpected since this is a comparison between
a leading order and a next-to-leading order generator.

The results, including all systematic and statistical uncertainties, 
are~$\mu = 1.14\pm0.27$ and~$\mu = 1.14^{+0.27}_{-0.26}$ for the
electron and muon channels respectively, yielding a combined result 
of~$\mu = 1.14^{+0.27}_{-0.26}$, which translates to
\begin{align}
  \sigma_{t\mathrm{-channel}} &= 100.1\pm3.34\mathrm{(stat)}^{+12.3}_{-12.3}\mathrm{(sys)}^{+20.0}_{-20.0}\mathrm{(th)}\unit{pb}\,\,\mathrm{(electron)},\\
  \sigma_{t\mathrm{-channel}} &= \phantom{1}99.8\pm3.30\mathrm{(stat)}^{+14.3}_{-12.6}\mathrm{(sys)}^{+18.2}_{-18.2}\mathrm{(th)}\unit{pb}\,\,\mathrm{(muon)},\\
  \sigma_{t\mathrm{-channel}} &= 100.1\pm2.22\mathrm{(stat)}^{+13.3}_{-13.3}\mathrm{(sys)}^{+19.1}_{-19.1}\mathrm{(th)}\unit{pb}\,\,\mathrm{(combined)}.
\end{align}
Here the theory uncertainty consists of the $t$-channel Powheg, top pair
\mcatnlo and W/Z+jets Alpgen variations and the \pdf uncertainty. The quoted
systematic consists of all other systematic uncertainties.

When the $t$-channel Powheg variation is ignored, 
%the uncertainties are 
%reduced by roughly a factor two, yielding 
a combined result 
of~$\sigma_{t\mathrm{-channel}} = 100.1\pm2.22\mathrm{(stat)}^{+13.3}_{-13.3}\mathrm{(sys)}^{+7.2}_{-7.2}\mathrm{(th)}\unit{pb}$ is found, where the
theory uncertainty is reduced by more than a factor two.
When the Powheg variation is used as nominal, instead of the \acermc
sample, the measured cross section goes down by $20\%$, which is not unexpected
due to the differences between to two generators, \acermc being leading order
and Powheg being a next-to-leading order generator.



%*****************************************
%*****************************************
%*****************************************
%*****************************************
%*****************************************
