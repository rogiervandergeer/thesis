%************************************************
\chapter{Single Top Quark Production}\label{ch:theory} % $\mathbb{ZNR}$
%************************************************

\section{Introduction}

The Standard Model~\cite{PhysRevLett.19.1264,PhysRevD.2.1285,Gaillard:1998ui,
    Novaes:1999yn}
(\standardmodel) of particle physics describes the
interactions of all known elementary particles. Despite being a very
complete and succesful theory, with the recent discovery of the Higgs boson
being its latest success~\cite{Aad:2012tfa,Chatrchyan:2012ufa},
      it is incomplete.
      For example, the \standardmodel does not contain gravity.
Also the hierarchy problem~\cite{peskin},
the baryon asymmetry in the universe~\cite{PhysRevLett.70.2833}
and the existence of dark matter~\cite{2012arXiv1201.3942P}
%and the lack of CP-violating interactions in
%the strong sector, which would explain the matter-antimatter asymmetry
%in the universe~\cite{sarkar:2007}, 
are reasons to look for new physics, or 
physics \emph{beyond the Standard Model}.

Many theories that aim to solve one or more of the problems with 
the \standardmodel exist.
Among the most notable ones are supersymmetry~\cite{Dawson:1996cq}
and string theory~\cite{Tong:2009np}.

The research presented in this thesis is motivated by the baryon asymmetry 
in the universe~\cite{PhysRevLett.70.2833}. The absence of anti-matter in our 
universe is still one of biggest questions in cosmology today. 
The conditions for the existence of a matter dominated universe, 
the baryogenesis, have been first described in~1967 by Andrei 
Sakharov~\cite{Sakharov:1967dj}: 
baryon number violation~\cite{PhysRevLett.37.8}, 
\textsc{c}- and \textsc{cp}-violation, 
and a departure from thermal equilibrium.

All three conditions are ingredients of the Standard Model as it passes 
through the electroweak phase transition during the formation of our universe. 
The \textsc{cp}-violation is experimentally well established in 
the $K$- and $B$-systems and described in the \standardmodel by the 
\textsc{cp}-violating phase~\cite{Kobayashi:1973fv} in the \textsc{ckm} 
matrix~\cite{1674-1137-38-9-090001}.
However, the \textsc{cp-}violation in the \standardmodel is far too small, 
by 10 orders of magnitude, to account for the baryon 
asymmetry~\cite{Hou:2008xd}. Hence, there must be \emph{new physics} to 
provide a substantial enhancement of the \textsc{cp}-violation in 
the \standardmodel.

In this thesis, two analyses are presented that search for such 
new physics processes: a direct search for an excited 
$b$ quark that realizes additional~\textsc{cp}-violation~\cite{Hou:2008xd}, 
and a model-independent search for \textsc{cp}-violation in the 
top quark production and decay vertex.

Interactions involving the top quark are an interesting 
and promising place to look
for new physics~\cite{Tait:2000sh}. 
Not only are there still many details of the top quark
to uncover (it was discovered only in 1995 at the \cdf and \dzero 
        experiments~\cite{PhysRevLett.74.2632,PhysRevLett.74.2626},
almost two decades after the discovery of the bottom quark), 
with its mass 
of~$m_\mathrm{t} = 173.2\pm0.9\unit{GeV}/c^2$~\cite{Lancaster:2011wr}
it is also the
heaviest known elementary particle.

\begin{figure}[t]
  \centering
  \includegraphics[width=0.8\textwidth]{figures/tt_xsec_vsroots}
  \caption{Predicted and measured \ttbar{} cross sections at the Tevatron and
the \lhc~\cite{sumtop}. 
All measurements show good agreement with the predictions.}
  \label{ttbarkplot}
\end{figure}


%New physics can influence top physics in a number of ways~\cite{Tait:2000sh}.
%Non-\standardmodel
%particles could influence the production or decay of the top quark.
%A fourth generation or excited quark could, for example, decay to 
%one or more top quarks. 
In section~\ref{sec:th.ex} an introduction
to a search for excited
quarks decaying to top quarks is given.
New physics can also influence top physics in more subtle ways,
    modifying processes and causing for example \textsc{cp}-violation.
Section~\ref{sec:thpol} introduces a model-independent search for
\textsc{cp}-violation.
%Non-\standardmodel particles could alter top quark production, for example
%if a fourth generation quark would decay to one or more top quarks, or if
%there were new bosons that coupled the top to other particles. New
%or modified \standardmodel interactions, such as flavor changing neutral
%currents, could show up in top physics. New physics may also be seen in the
%polarization of the top, which is the only quark of which the polarization
%can be measured~\cite{Chen2005126}.

\section{Single top production channels}

\begin{figure}[t]
  \centering
  \includegraphics[width=0.8\textwidth]{figures/singletop_allchanvsroots_ATLASonly}
  \caption{Predicted and measured single top cross sections at the
the \lhc~\cite{sumtop}. 
All measurements show good agreement with the predictions.}
  \label{stopkplot}
\end{figure}



In a proton-proton collider like the \lhc (see section~\ref{sec:LHC})
top quarks are produced through a multitude of processes. Top-antitop pair
production ($t\bar{t}$) 
    is the process with the highest cross section. 
Figures~\ref{ttbarkplot} shows the cross sections of \ttbar{} production
at the \lhc and the Tevatron. 
Single top production processes, where a single top quark is produced, have
smaller cross sections but provide complementary and essential information
on the~$Wtb$-vertex.
    These processes can be divided  into three \emph{channels}:
    t-channel, s-channel and associated Wt production (or 
    Wt-channel\footnote{In some literature this is also called tW-channel.}).
Feynman diagrams of the $t\bar{t}$, s- and t-channel and Wt associated 
production processes are shown in 
figures~\ref{fig:ttbardiag},~\ref{fig:stchandiag} and~\ref{fig:wtchandiag}
respectively, and the cross sections of these processes are shown in
figure~\ref{stopkplot}.

\begin{figure}[p]
  \phantom{.}\hfill
  \begin{subfigure}{0.32\textwidth}
    \includegraphics[scale=0.7]{figures/ttA}
  \end{subfigure}
  \hfill
  \begin{subfigure}{0.32\textwidth}
    \includegraphics[scale=0.7]{figures/ttB}
  \end{subfigure}
  \hfill
  \begin{subfigure}{0.32\textwidth}
    \includegraphics[scale=0.7]{figures/ttC}
  \end{subfigure}
  \hfill\phantom{.}
  \caption{Leading order top-antitop quark pair production Feynman diagrams.}
  \label{fig:ttbardiag}
\end{figure}

\begin{figure}[p]
  \phantom{.}\hfill
  \begin{subfigure}{0.32\textwidth}
    \includegraphics[scale=0.8]{figures/sch}
    \caption{s-channel}
  \end{subfigure}
  \hfill
  \begin{subfigure}{0.32\textwidth}
    \includegraphics[scale=0.8]{figures/tchB}
    \caption{t-channel}
    \label{fig:tchandiag}
  \end{subfigure}
  \hfill\phantom{.}
  \caption{Leading order s-channel and t-channel 
      production Feynman diagrams.}
  \label{fig:stchandiag}
\end{figure}

\begin{figure}[p]
  \phantom{.}\hfill
  \begin{subfigure}{0.32\textwidth}
    \includegraphics[scale=0.8]{figures/WtB}
  \end{subfigure}
  \hfill
  \begin{subfigure}{0.32\textwidth}
    \includegraphics[scale=0.8]{figures/WtA}
  \end{subfigure}
  \hfill\phantom{.}
  \caption{Leading order Wt associated production Feynman diagrams.}
  \label{fig:wtchandiag}
\end{figure}

Due to the larger center-of-mass energy, the cross sections of the top 
production processes are larger at the \lhc than at the Tevatron, where the
top quark has been discovered. As can be seen in table~\ref{tab:xsec},
the cross sections of the four processes do not grow with the same factor:
while $t\bar{t}$ and single top t-channel production
gain roughly a factor of 30 from Tevatron
to \lhc, Wt-channel gains more than two orders of magnitude and the
s-channel cross section grows by a factor of four. This is the result of
a difference
in nature of the colliding particles:
Tevatron was a~$p\bar{p}$ collider, while the \lhc is a~$pp$ collider.
The colliders therefore probe different combinations of valence and sea quark
distributions.

\begin{table}[htb]
  \centering
  \begin{tabular}{c | r@{$\pm$}l@{ }c | r@{$\pm$}l@{ }c r@{$\pm$}l@{ }c }
    & \multicolumn{3}{c}{Tevatron} & \multicolumn{6}{c}{\lhc} \\
    \multicolumn{1}{r|}{$\sqrt{s}$} & \multicolumn{3}{c}{$1.96\unit{TeV}$} &
    \multicolumn{3}{c}{$7\unit{TeV}$} &
    \multicolumn{3}{c}{$8\unit{TeV}$} \\\hline
    $t\bar{t}$ & $7.60$&$0.41$ & \cite{Aaltonen:2013wca} 
    & $173.3$&$10.1$ & \cite{ATLAS-CONF-2012-134} 
    & $237.7$&$32$ & \cite{ATLAS-CONF-2013-097} \\
    s-channel  & $1.29$&$0.25$ & \cite{CDF:2014uma} & 
    $4.6$&$0.2^\dag$ & \cite{Kidonakis:2010tc} 
    & $5.6$&$0.2^\dag$ & \cite{Kidonakis:2010tc} \\
    t-channel  & $2.26$&$0.12^\dag$ &\cite{Garcia-Bellido:2013rna} &
    $82.7$&$13.1$&\cite{ATLAS-CONF-2012-056}&
    $85$&$12$&\cite{CMS-PAS-TOP-12-002}\\
    Wt-channel & $0.14$&$0.03^\dag$ &\cite{PhysRevD.74.114012} &
    $16.8$&$5.7$ & \cite{Aad:2012xca} & 
    $23.4$&$5.5$&\cite{CMS-PAS-TOP-12-040} \\
  \end{tabular}
  \caption{Cross section (in picobarn) of the top production processes
      at the Tevatron and at two center-of-mass energies at the \lhc.
      The center-of-mass energy is not the only difference between 
      the machines, as the Tevatron was a proton-antiproton machine, and
      the \lhc is a proton-proton collider.
      Where available, observed cross sections are shown.
        Cross sections noted with a dagger$^\dag$
        are theoretical cross sections.}
  \label{tab:xsec}
\end{table}

% t-chan sm:
% 65.9 \pm 2.6 (7 TeV) 78.2 \pm 2.7 (8 TeV) arXiv 1311.0283
%wt-chan sm:
% 7.8 \pm 0.6 (7TeV) 11.1 \pm 0.8 (8TeV) arXiv 1311.0283


\section{Excited quarks}
\label{sec:th.ex}

Some beyond the Standard Model physics models feature new heavy quarks, 
such as a fourth generation of quarks. The new quarks in such models have,
in addition to weak couplings, new strong couplings such as technicolor or
 vector-like  couplings. 
 These models intend to address, for example, the hierarchy problem
 or the baryon asymmetry in the universe~\cite{Hill:2002ap,Martin:2009bg,Kumar:2010vx,Holdom:2009rf,Alok:2010zj}.

\begin{figure}[hp]
  \centering
  \includegraphics[scale=0.8]{figures/bpr}
  \caption{Excited bottom quark production and decay Feynman diagram.}
  \label{fig:bprime}
\end{figure}

The Wt-channel signature is sensitive to heavy particles decaying to a
top quark and a W~boson. An example of such a particle is a heavy excited
bottom-like quark~($\bstar$) which is resonantly
produced through the fusion of a bottom quark
and a gluon~\cite{kutsite,Baur:1987ga} and which decays to a top~quark and a 
W~boson~\cite{Nutter:2012an}. A Feynman diagram of this process is shown in
figure~\ref{fig:bprime}.

The Langrangian describing the chromomagnetic coupling through which the~\bstar
is created is
given by
\begin{equation}
\mathcal{L} = \frac{g_s}{2\Lambda}G_{\mu\nu}\bar{b}\sigma^{\mu\nu}
\left(\kappa_L^{\bstar}P_L + \kappa_R^{\bstar}P_R\right)\bstar +\mathrm{h.c.},
\end{equation}
where~$g_s$ is the strength of the strong coupling,~$G_{\mu\nu}$ is the gauge
field tensor of the gluon~$\Lambda = M_{\bstar}$ is the scale of new
physics,~$P_L$ and~$P_R$ are the projection operators and~$\kappa_L^{\bstar}$
and~$\kappa_R^{\bstar}$ are the left- and right-handed coupling strengths,
    respectively.
This chromomagnetic coupling 
leads to a relatively high production cross section.
%\, of the order of
%hundreds of picobarns at the \lhc.

The Lagrangian describing the electroweak decay of the~\bstar is given
by
\begin{equation}
\mathcal{L} = \frac{g_2}{\sqrt{2}}W_\mu^+\bar{t}\gamma^\mu
\left(g_LP_L+g_RP_R\right)\bstar +\mathrm{h.c.},
\end{equation}
where~$g_2$ is the strength of the \textsc{su}(2)$_L$ weak coupling and~$g_L$
and~$g_R$ are the respective coupling
strengths for the left- and right-handed couplings.
For all \standardmodel processes, the electroweak coupling is purely 
left-handed, but for the~\bstar it could left-handed, right-handed or
vector-like\footnote{A vector-like coupling has non-zero coupling strengths
    for the left- and right-handed couplings.}.

The branching ratios of the excited quark are shown in figure~\ref{fig:bpbr}.
For a left-handed coupling the decay is dominated by $\bstar\rightarrow Wt$ for
masses above $400\unit{GeV}/c^2$. For masses below $400\unit{GeV}/c^2$, 
       $\bstar\rightarrow Zb$ is the primary decay channel.

\begin{figure}[hpt]
  \centering
  \begin{subfigure}{\textwidth}
    \centering
    \includegraphics[width=0.7\textwidth]{figures/bprimebranch}
    \caption{Branching ratio of the left-handed decay of an
        excited bottom quark.}
    \label{fig:bpbr}
  \end{subfigure}\\
  \vskip 12mm
  \begin{subfigure}{\textwidth}
    \centering
    \includegraphics[width=0.7\textwidth]{figures/bprimecross}
    \caption{Cross section times branching ratio of the production of an
        excited bottom quark and the decay to Wt at the \lhc.}
    \label{fig:bpxs}
  \end{subfigure}
  \caption{Branching ratio and cross section of the left-handed
      production and decay of an excited bottom quark
      as a function of its mass~\cite{Nutter:2012an}.}
  \label{fig:bpbrxs}
\end{figure}

The cross section of the excited quark production and decay to Wt at the
\lhc is shown
in figure~\ref{fig:bpxs}. At~$\sqrt{s} = 7\unit{TeV}$ and for a mass
of~$m_{\bstar} = 900\unit{GeV}/c^2$, the cross section times branching ratio
is~$0.80\unit{pb}$, which is of the order of $5\%$ of the \standardmodel
Wt-channel production cross section at that center-of-mass energy.
For masses below~$m_{\bstar} = 500\unit{GeV}/c^2$, the cross section times
branching ratio of~$\bstar\rightarrow Wt$ is larger than the \standardmodel
Wt-channel cross section. 
The width of the \bstar is small enough to recognize a \bstar signal by
its mass. 
This makes the Wt-channel a good candidate for a
search for excited quarks, or other new heavy quarks that decay to~Wt.
In chapter~\ref{ch:bprime} a search for the \bstar is presented.


\section{Polarization in t-channel single top}
\label{sec:thpol}

Due to its very high mass and its decay width 
of~$2\unit{GeV}$~\cite{1674-1137-38-9-090001} the top quark has a lifetime of
the order of $10^{-25}\unit{s}$. As this is much shorter than the timescale
of {\sc qcd} interactions, the top quark does not hadronize before 
decay~\cite{Bigi1986157}.
The top quark is therefore the only quark of which the spin can be 
measured. % before it is randomized.
Single top quarks produced in the \lhc are strongly 
polarized~\cite{Mahlon:1999gz}, and this polarization is sensitive to new
physics, such as supersymmetry~\cite{Chen2005126}.

A general, minimal, parameterization of the Wtb vertex arising from
%dimension-six 
effective operators of maximum dimension six can be written as~\cite{AguilarSaavedra:2008zc}
\begin{equation}
\begin{aligned}
 \mathcal{L}_{Wtb} = &-\frac{g}{\sqrt{2}}\bar{b}\gamma^\mu\left(V_LP_L+V_RP_R\right)
    t W_\mu^-\\
        &-\frac{g}{\sqrt{2}}\bar{b}\frac{i\sigma^{\mu\nu}q_\nu}{M_W}
            \left(g_LP_L + g_RP_R\right)tW_\mu^- +\mathrm{h.c.},
\end{aligned}
\end{equation}
where~$V_L$ and~$V_R$ are the coupling strengths of the left- and right-handed
vector couplings repectively, and~$g_L$ and~$g_R$ are the left- and right-handed
tensor couplings respectively. In the \standardmodel,~$V_L=1$ 
while~$V_R = g_L = g_R = 0$. 

Here~$g_R$ is particularly interesting, as a non-zero imaginary part of~$g_R$
implies \textsc{cp}-violation. 
%The imaginary part of~$g_R$ can be measured
%through the polarization of the top quark and the~$W$ boson, as explained
%below.
The imaginary part of~$g_R$ is experimentally accessible through the angular
distribution of the top quark and its decay products, as explained below.

\subsection{Top quark polarization}
%As the top quark decays too quickly to be observed directly, the top quark
%polarization is measured using the angular distributions of the decay products.
The angular distribution of any decay product of the top quark is given by
\begin{equation}
 \frac{1}{\Gamma} \frac{d\Gamma}{d\cos\theta} = \frac{1}{2} \left(
         1+P\alpha \cos\theta\right),
  \label{eq:pol}
\end{equation}
where $\theta$ is the angle between the momentum of the decay product in
the top quark rest frame and the top spin direction, $\Gamma$ is the decay
width of the top quark, the constant $\alpha$
is the \emph{spin analyzing power} of the decay product, and $P$ is the 
degree of polarization of the top quark~\cite{AguilarSaavedra:2010nx}.
In the decay $t \rightarrow W^+\,b \rightarrow \ell^+\,\nu_\ell\,b$,
the spin analyzing power for the decay products are shown in table~\ref{tab:sp}.
%The charged lepton is the most sensitive spin analyzer.

\begin{table}[hbp]
  \centering
%  \hfill
%  \begin{subtable}{0.45\linewidth}
    \centering
    \begin{tabular}{l@{$\,=\,$}r@{.}l}
      $\alpha_{\ell^+}$ & $1$&$000$ \\
      $\alpha_{b}$ & $-0$&$403$ \\
      $\alpha_{W^+}$ & $0$&$403$ \\
      $\alpha_{\nu_\ell}$ & $-0$&$324$ \\
%      $\alpha_{\ell^+}$ & $0$&$998$ \\
%      $\alpha_{b}$ & $-0$&$39$ \\
%      $\alpha_{W^+}$ & $0$&$39$ \\
%      $\alpha_{\nu_\ell}$ & $-0$&$33$ \\
    \end{tabular}
%    \caption{}
%    \label{tab:spt}
%  \end{subtable} \hfill
%  \begin{subtable}{0.45\linewidth}
%    \centering
%    \begin{tabular}{l@{$\,=\,$}r@{.}l}
%      $\alpha_{\ell^-}$ & $-0$&$998$ \\
%      $\alpha_{\bar{b}}$ & $0$&$39$ \\
%      $\alpha_{W^-}$ & $-0$&$39$ \\
%      $\alpha_{\bar{\nu}_\ell}$ & $0$&$33$ \\
%    \end{tabular}
%    \caption{}
%    \label{tab:spa}
%  \end{subtable} \hfill
  \caption{Leading order spin analyzing power for all decay products of the top 
      quark~\cite{Mahlon:1999gz}. The corresponding quantities
  for anti top quarks have equal values with opposite signs.}
%      (table~\subref{tab:spt}) and of the anti-top quark 
%          (table~\subref{tab:spa})
  \label{tab:sp}
\end{table}

The decay of a polarized top quark is visualized in 
figure~\ref{fig:spins}. 
The~$W$ boson is either left-handed or longitudinal; a 
right-handed~$W$ boson would require the bottom quark to have 
spin-$\frac{2}{3}$ and is therefore not allowed. The momentum of a
longitudinal~$W$ boson is parallel to the spin of the top quark,
while the momentum of a left-handed~$W$ boson is antiparallel to the
top quark spin. Since the neutrino is always left-handed, the momentum
of the charged lepton is always in the direction of the spin of the~$W$ boson
in the~$W$ boson rest frame. 
%The spin of the bottom quark is either parallel or
%antiparallel to the spin of the top quark, and its helicity,
%the projection of its spin onto its momentum, is always
%left-handed (or antiparallel). The~$W$ boson is either left-handed or 
%longitudinal. The momentum of the charged lepton is always in the direction
%of the spin of the~$W$ boson in the~$W$ boson rest frame, since the neutrino
%has to be left-handed. 
As a result, and due to the momentum of the~$W$ boson
in the top quark rest frame, the projection of the
momentum of the charged lepton in the top
quark rest frame onto the top quark spin is always parallel to this spin.
This means that the charged lepton is the most sensitive spin analyzer.



\begin{figure}[hpt]
  \centering
  \hfill
  \begin{subfigure}{0.45\textwidth}
    \centering
    \includegraphics[height=8cm]{figures/spins1}
    \caption{}
    \label{fig:spins1}
  \end{subfigure}
  \hfill
  \begin{subfigure}{0.45\textwidth}
    \centering
    \includegraphics[height=8cm]{figures/spins2}
    \caption{}
    \label{fig:spins2}
  \end{subfigure}
  \hfill
  \caption{Spin in the decay of a top quark. Only two configurations
  are possible: in~\subref{fig:spins1} the direction of the momentum
  of the~$W$ boson is parallel to the spin of the top quark, while
  in~\subref{fig:spins2} it is antiparallel. }%The momentum of the charged
%   lepton is always parallel to the spin of the~$W$ boson in the~$W$ boson
%  rest frame. In}
  \label{fig:spins}
\end{figure}


\smallskip

The degree of polarization of the top quark depends on the chosen spin basis.
The top quark is highly polarized in
the helicity basis and the spectator basis. In the helicity basis
the spin quantization axis is chosen along the
direction of the momentum of the top quark in the center-of-mass frame,
while in the spectator basis the spin quantization axis is chosen along
the direction of the momentum of the spectator quark\footnote{
The spectator quark is the light quark produced in a t-channel diagram. In
figure~\ref{fig:tchandiag},~$q'$ is the spectator.} in the top quark rest
frame~\cite{Schwienhorst:2010je}. If the center-of-mass frame is
calculated using only the top quark and the spectator jet
these two bases are equal but inverted.

%These two bases are equal (but inverted)
%if the center-of-mass frame is calculated using only the top quark and the
%spectator jet.

Figures~\ref{fig:basis1}-\subref{fig:thetaLN} show the definitions of
several polarization-sensitive angles.
The most sensitive angle which can be used to measure the polarization
using equation~\ref{eq:pol},~$\thetax$,
  which uses the charged lepton as the spin analyzer in the spectator basis,
      is shown in figure~\ref{fig:thetaX}.
%Figure~\ref{fig:thetaX} shows the definition of~$\thetax$, the most sensitive
%angle which can be used to measure the polarization using equation~\ref{eq:pol},
An alternative angle, using the~$W$ boson as spin analyzer, is shown in
figure~\ref{fig:thetaW}. This angle is much less sensitive due to the
lower spin analyzing power of the~$W$ boson.

\begin{figure}[p]
  \centering
  \hfill
  \begin{subfigure}{0.45\linewidth}
    \centering
    \includegraphics[width=0.90\textwidth]{figures/basis}
    \caption{Definition of~$\hat{N}$ and~$\hat{T}$: 
        $\hat{N}=\vec{p}_q\times\vec{p}_W$ 
    and~$\hat{T}=\vec{p}_W\times\hat{N}$, where~$\vec{p}_q$ 
    and~$\vec{p}_W$ are the momenta of respectively the 
    spectator quark and the~$W$ boson in the top quark rest frame.}
    \label{fig:basis1}
  \end{subfigure}
  \hfill
  \begin{subfigure}{0.45\linewidth}
    \centering
    \includegraphics[width=0.90\textwidth]{figures/thetaX}
    \caption{Angle \thetax between the momenta of the spectator 
        quark~($\vec{p}_q$) and the charged lepton~($\vec{p}_\ell$), both
    in the top quark rest frame.\\\phantom{-}}
    \label{fig:thetaX}
  \end{subfigure}
  \hfill \\
  \hfill
  \begin{subfigure}{0.45\linewidth}
    \centering
    \includegraphics[width=0.90\textwidth]{figures/thetaW}
    \caption{Angle \thetaW between the momenta of the spectator
        quark~($\vec{p}_q$) and the~$W$ boson~($\vec{p}_W$), both
    in the top quark rest frame.}
    \label{fig:thetaW}
  \end{subfigure}
  \hfill
  \begin{subfigure}{0.45\linewidth}
    \centering
    \includegraphics[width=0.90\textwidth]{figures/thetaS}
    \caption{Angle \thetas between the momenta of the $W$ boson in
    the top quark rest frame~($\vec{p}_W$) and of the charged lepton
    in the~$W$ boson rest frame~($\vec{p}_\ell$).}
    \label{fig:thetaS}
  \end{subfigure}
  \hfill \\
  \hfill
  \begin{subfigure}{0.45\linewidth}
    \centering
    \includegraphics[width=0.90\textwidth]{figures/thetaLT}
    \caption{Angle \thetalt between the momentum of the charged lepton
    in the~$W$ boson rest frame~($\vec{p}_\ell$) and the transverse axis.}
    \label{fig:thetaLT}
  \end{subfigure}
  \hfill
  \begin{subfigure}{0.45\linewidth}
    \centering
    \includegraphics[width=0.90\textwidth]{figures/thetaLN}
    \caption{Angle \thetaln between the momentum of the charged lepton
    in the~$W$ boson rest frame~($\vec{p}_\ell$) and the normal axis.}
    \label{fig:thetaLN}
  \end{subfigure}
  \hfill 
  \caption{Definition of the spin axes~$\hat{N}$ and~$\hat{T}$ and the
  angles~\thetax,~\thetaW,~\thetas,~\thetalt and~\thetaln.}
  \label{fig:bases}
\end{figure}

\subsection{$W$ boson polarization}
\label{sec:wbpol}
The polarization of the~$W$ boson decay product of top quarks can
also be measured. This is done using the distribution of the angle
between the lepton
producted in the~$W$ boson decay and a spin axis.

%The polarization of the $W$ boson decay product of polarized top quarks can
%also be measured using the distribution of the lepton produced in the W boson
%decay with respect to a spin axis.

The helicity, the spin along the momentum axis, of the~$W$ boson can
be measured using the distribution of the angle between the~$W$ boson
momentum axis in the top quark rest frame and the momentum of the charged
lepton in the~$W$ boson rest frame, as shown in figure~\ref{fig:thetaS}.
For polarized top quarks
   % n addition to the~$W$ boson momentum axis
two alternative spin axes can be considered: the \emph{normal}
axis~($\hat{N}$), perpendicular to the momentum of the W boson in the top quark
rest frame and the top quark spin direction
    and the \emph{transverse} axis~($\hat{T}$), perpendicular to both the normal
axis and the W momentum in the top quark rest frame 
as defined in figure~\ref{fig:basis1}. 
%The corresponding angles are shown in
%figures~\ref{fig:thetaLT} and~\subref{fig:thetaLN}.

In the case of fully polarized top quarks, the angular distribution is given by
\begin{equation}
  \frac{1}{\Gamma} \frac{d\Gamma}{d(\cos\theta)} = 
  \frac{3}{8}\left(1+\cos\theta\right)^2 \fplus + 
  \frac{3}{8}\left(1-\cos\theta\right)^2 \fminus + 
  \frac{3}{4}\left(\sin\theta\right)^2  \fzero,
  \label{eq:wpol}
\end{equation}
where~$\theta$ is the angle between the momentum of the lepton
in the $W$ boson rest frame and the reference axis (either the transverse
or the normal axis, see figures~\ref{fig:thetaLT} 
and~\subref{fig:thetaLN}),~$\Gamma$ is the decay width of the $W$ boson,
and~\fplus,~\fminus and~\fzero are the polarization fractions corresponding
to right-handed, left-handed and longitudinal helicities respectively.
Each term is maximal~($=3/4$) when the momentum of the lepton is parallel
to the direction of the spin:~$\theta=0$ for right-handed,~$\theta=\pi$ for 
left-handed and~$\theta=\pi/2$ for longitudinal helicities.
The polarization fractions satisfy
\begin{equation}
\fplus + \fminus + \fzero = 1.
\label{eq:sumrule}
\end{equation}
The \standardmodel predictions for the polarization fractions are
listed in table~\ref{tab:smfrac}. 

When the top quarks are not fully polarized (i.e.\ $P < 1$), the polarization
fractions in equation~\ref{eq:wpol} are replaced by effective polarization
fractions depending on~$P$:
\begin{align}
  \tfplus &= \frac{1+P}{2}\fplus + \frac{1-P}{2}\fminus,\\
  \tfminus &= \frac{1+P}{2}\fminus + \frac{1-P}{2}\fplus,\\
  \tfzero\, &= \fzero.\phantom{\frac{1-P}{1-P}}
\end{align}
%While the polarization fractions for top
%decays in the normal basis are equal to those for antitop decays, the
%polarization fractions for antitop decays 

\begin{table}[tp]
  \centering
  \hfill
  \begin{subtable}{0.45\linewidth}
    \centering
    \begin{tabular}{l@{$\,=\,$}r@{.}l}
      $\fplusn$ & $0$&$426$ \\
      $\fminusn$ & $0$&$426$ \\
      $\fzeron$ & $0$&$149$ \\
    \end{tabular}
    \caption{}
    \label{tab:smfracn}
  \end{subtable} \hfill
  \begin{subtable}{0.45\linewidth}
    \centering
    \begin{tabular}{l@{$\,=\,$}r@{.}l}
      $\fplust$ & $0$&$679$ \\
      $\fminust$ & $0$&$172$ \\
      $\fzerot$ & $0$&$149$ \\
    \end{tabular}
    \caption{}
    \label{tab:smfract}
  \end{subtable} \hfill
  \caption{The \standardmodel predictions at tree-level for the 
      polarization fractions in the normal basis (\subref{tab:smfracn})
    and the transverse basis (\subref{tab:smfract})
    \cite{AguilarSaavedra:2010nx}.}
  \label{tab:smfrac}
\end{table}

From the polarization fractions we can construct the forward-backward 
asymmetry in the angular distributions as follows:
\begin{align}
  \afbn &= \frac{3}{4} \left(\tfplusn-\tfminusn\right) 
         = \frac{3}{4}P\left(\fplusn -\fminusn\right),\\
  \afbt &= \frac{3}{4} \left(\tfplust-\tfminust\right) 
         = \frac{3}{4}P\left(\fplust -\fminust\right).
\end{align}
While \afbt is sensitive to the real parts of anomalous $Wtb$-couplings,
\afbn is sensitive to the imaginary parts. It is particularly sensitive
to the imaginary part of the right-handed tensor coupling~$\Im(g_R)$.
When~$V_R = g_L = 0$ and~$V_L = 1$, like in the \standardmodel, 
\begin{equation}
  \afbn = 0.64\,P\,\Im(g_R),
  \label{eq:imgr}
\end{equation}
for small~$g_R$~\cite{AguilarSaavedra:2010nx}. When combining
top and antitop decays, a non-zero \afbn implies
\textsc{cp}-violation. Hence this quantity provides a promising means to
study anomalous couplings, and a search for \textsc{cp}-violation is
described in chapter~\ref{ch:polarization}.

\section{Summary}

Although the \standardmodel is very complete and successful,
there are several reasons to look for new physics. 
Processes involving the top quark are interesting to study in search
for new physics, for example because of the very high mass of the top quark.
The single top t- and Wt-channels are especially good candidates for
searches for new physics, because they are sensitive to the polarization
of the top quark and to new, heavy particles decaying to signatures similar
to these channels.

%*****************************************
%*****************************************
%*****************************************
%*****************************************
%*****************************************
