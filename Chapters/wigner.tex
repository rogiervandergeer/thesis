%********************************************************************
% Appendix
%*******************************************************
% If problems with the headers: get headings in appendix etc. right
%\markboth{\spacedlowsmallcaps{Appendix}}{\spacedlowsmallcaps{Appendix}}
\chapter{Wigner rotation}
\label{ch:wigner}

Classical Galilean transformations are composed of translations
and rotations.
Galilean translations and rotations each form a mathematical group. The
combination of two successive translations is a translation, and the
combination of two successive rotations is a rotation. 
In relativistic space-time this is no longer the case: two successive 
Lorentz boosts are only equal to a Lorentz boost if the directions of the
boosts are parallel, and hence Lorentz boosts do not form a group.


\smallskip
In order to see this we first have a look at the nature of a Lorentz boost. 
The Lorentz boost with velocity $\beta$ in the $x$-direction can be written
as:
\begin{equation}
\left(\begin{matrix} ct' \\ x'\\y'\\z'\end{matrix}\right)
= 
\left(\begin{matrix}
 \gamma_1 & -\gamma_1\beta_1 & 0 & 0 \\
 -\gamma_1\beta_1 & \gamma_1 & 0 & 0 \\
 0 & 0 & 1 & 0 \\
 0 & 0 & 0 & 1 \\
\end{matrix}\right)
\left(\begin{matrix} ct \\ x\\y\\z\end{matrix}\right),
\end{equation}
where we see that this involves a symmetric matrix. If $B_x$ is the boost along
the $x$-axis then we can construct a boost in any direction by rotating
$B_x$ using a rotation matrix~$R$: $B' = R B_x R^{-1}$. As the result of 
rotating a symmetric matrix is also a symmetric matrix\footnote{Any matrix $B$
congruent to a symmetric matrix $A$ is symmetric ($B$ is symmetric 
if~$B = RAR^T$ for any matrix $R$). As rotations are orthogonal ($R^T R = I$
    for any rotation $R$),
any matrix obtained by rotating a symmetric matrix is symmetric.
},~$B'$ is symmetric.
As any boost can be constructed with combination of a rotation~$R$ 
and a boost along the~$x$-axis $B_x$, the matrix of any Lorentz 
boost is symmetric.

Now we consider two boosts along directions that are not parallel.
For simplicity we look at orthogonal boosts along the $x$- and $y$-axes:
 $B_1$ and~$B_2$ with boost
vectors~$\vec{\beta}_1 = \beta_1\hat{x}$ 
and~$\vec{\beta}_2 = \beta_2\hat{y}$ respectively:
\begin{equation}
B_1 = 
\left(\begin{matrix}
 \gamma_1 & -\gamma_1\beta_1 & 0 & 0 \\
 -\gamma_1\beta_1 & \gamma_1 & 0 & 0 \\
 0 & 0 & 1 & 0 \\
 0 & 0 & 0 & 1 \\
\end{matrix}\right),\,\,\, B_2 = 
\left(\begin{matrix}
 \gamma_2 & 0 & -\gamma_2\beta_2 & 0 \\
 0 & 1 & 0 & 0 \\
 -\gamma_2\beta_2 & 0 & \gamma_2 & 0 \\
 0 & 0 & 0 & 1 \\
\end{matrix}\right).
\end{equation}
Consecutive application of these boosts results in:
\begin{equation}
  B_2\,B_1 = \left(\begin{matrix}
    \gamma_1\gamma_2 & -\gamma_1\gamma_2\beta_1 & - \gamma_2 \beta_2 & 0 \\
    -\gamma_1\beta_1 & \gamma_1 & 0 & 0 \\
    -\gamma_1\gamma_2\beta_2 & \gamma_1\gamma_2\beta_1\beta_2 & \gamma_2 & 0\\
    0 &0 & 0& 1\\
\end{matrix}\right),
\end{equation}
which is clearly an asymmetrical matrix, and hence can not be a Lorentz boost.
The result can however be written as the combination of a successive 
boost~$B_{12}$ and a rotation $R$ called a 
\emph{Wigner rotation}~\cite{wigner,0143-0807-20-3-003}. 
In this specific case~$R$ is a rotation
in the~$x$-$y$-plane with angle 
\begin{equation}
\theta = \arctan\left(-\frac{\gamma_1\gamma_2\beta_1\beta_2}{\gamma_1+\gamma_2}
        \right),
\end{equation}
given by
\begin{equation}
R = \left(\begin{matrix}
1&0&0&0\\
0&\cos\theta&\sin\theta&0\\
0&-\sin\theta&\cos\theta&0\\
0&0&0&1\\
        \end{matrix}\right),
\end{equation}
and $B_{12}$, which obeys $R B_{12} = B_2 B_1$, is given by the symmetric 
matrix:
\begin{equation}
B_{12} = \left(\begin{matrix}
\gamma_1\gamma_2 & -\gamma_1\gamma_2\beta_1 & -\gamma_2\beta_2 & 0 \\
-\gamma_1\gamma_2\beta_1 & -\gamma_2\cos\theta & -\gamma_2\sin\theta & 0 \\
-\gamma_2\beta_2 & -\gamma_2\sin\theta & \gamma_2\cos\theta & 0\\
0 & 0 & 0 & 1\\
\end{matrix}\right).
\end{equation}
From this matrix we find that $\gamma_{12} = \gamma_1\gamma_2$ 
and~$\beta_{12} = \beta_1 \hat{x} + \beta_2/\gamma_1\hat{y}$.
A graphical representation of the given example is shown 
in figure~\ref{fig:wigner}.

\begin{figure}[ht]
  \centering
  \includegraphics[width=0.7\textwidth]{figures/wigner}
  \caption{A graphical representation of the Wigner rotation for 
  $\vec{\beta}_1 = 0.7\hat{x}$ and $\vec{\beta}_2 = 0.7\hat{y}$. When the
  original vector is boosted with~$B_1$ and~$B_2$ successively the resulting
  vector (shown in blue) is rotated with respect to the result of the 
  combined boost $B_{12}$ (shown in red).
  Note that, although printed diagonally, boost $B_{12}$ has unequal $x$- and
  $y$-components: $\vec{\beta}_{12} = 0.7\hat{x} + 0.5\hat{y}$.}
  \label{fig:wigner}
\end{figure}

Since any rotation can be combined with the Wigner rotation to form one
rotation, Lorentz \emph{transformations}, combinations
of a Lorentz boost and a rotation, do form a group. 

%\smallskip
%In particle physics analyses one should take care when boosting events from
%one Lorentz frame to another.
